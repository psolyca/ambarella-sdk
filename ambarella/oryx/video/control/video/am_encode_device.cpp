/*******************************************************************************
 * am_encode_device.cpp
 *
 * History:
 *   May 7, 2015 - [ypchang] created file
 *
 * Copyright (c) 2016 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

#include "am_base_include.h"
#include "am_log.h"
#include "am_define.h"

#include "am_video_types.h"
#include "am_encode_types.h"
#include "am_video_utility.h"
#include "am_encode_config.h"
#include "am_encode_device.h"

#include "am_vin.h"
#include "am_vout.h"
#include "am_mutex.h"
#include "am_encode_buffer.h"
#include "am_encode_stream.h"

#include "am_plugin.h"
#include "am_encode_plugin_if.h"
#include "am_encode_warp_if.h"
#include "am_low_bitrate_control_if.h"
#include "am_encode_eis_if.h"
#include "am_encode_overlay_if.h"
#include "am_dptz_if.h"

AMVideoPlugin::AMVideoPlugin() :
  so(nullptr),
  plugin(nullptr)
{}

AMVideoPlugin::AMVideoPlugin(const AMVideoPlugin &vplugin) :
  so(vplugin.so),
  plugin(vplugin.plugin)
{}

AMVideoPlugin::~AMVideoPlugin()
{
  AM_DESTROY(plugin);
  AM_DESTROY(so);
}

AMEncodeDevice* AMEncodeDevice::create()
{
  AMEncodeDevice *result = new AMEncodeDevice();
  if (result && (AM_RESULT_OK != result->init())) {
    delete result;
    result = nullptr;
  }
  return result;
}

void AMEncodeDevice::destroy()
{
  delete this;
}

AM_RESULT AMEncodeDevice::init()
{
  AM_RESULT result = AM_RESULT_OK;

  do {
    if (!(m_platform = AMIPlatform::get_instance())) {
      result = AM_RESULT_ERR_MEM;
      ERROR("Failed to create AMIPlatform!");
      break;
    }

    if ((result = vin_create()) != AM_RESULT_OK) {
      ERROR("Failed to create VIN!");
      break;
    }

    if (!(m_vout = AMVout::create())) {
      ERROR("Failed to create VOUT!");
      result = AM_RESULT_ERR_MEM;
      break;
    }

    if (!(m_iav_lock = AMMutex::create())) {
      ERROR("Failed to create mutex for IAV!");
      result = AM_RESULT_ERR_MEM;
      break;
    }

    if (!(m_buffer = AMEncodeBuffer::create())) {
      result = AM_RESULT_ERR_MEM;
      ERROR("Failed to create AMEncodeBuffer!");
      break;
    }

    if (!(m_stream = AMEncodeStream::create())) {
      result = AM_RESULT_ERR_MEM;
      ERROR("Failed to create AMEncodeStream!");
      break;
    }

    if ((result = load_config_all()) != AM_RESULT_OK) {
      break;
    }

    load_all_plugins();
  } while (0);

  return result;
}

AMEncodeDevice::AMEncodeDevice() :
  m_buffer(nullptr),
  m_stream(nullptr),
  m_vout(nullptr),
  m_iav_lock(nullptr),
  m_platform(nullptr),
  m_state(AM_IAV_STATE_INIT)
{
  m_vin.clear();
  m_plugin_map_encode.clear();
  m_plugin_map_preview.clear();
  DEBUG("AMEncodeDevice is created!");
}

AMEncodeDevice::~AMEncodeDevice()
{
  for (AMVideoPluginMap::iterator iter = m_plugin_map_encode.begin();
      iter != m_plugin_map_encode.end(); ++ iter) {
    delete iter->second;
  }
  for (AMVideoPluginMap::iterator iter = m_plugin_map_preview.begin();
      iter != m_plugin_map_preview.end(); ++ iter) {
    delete iter->second;
  }
  m_plugin_map_encode.clear();
  m_plugin_map_preview.clear();
  vin_delete();
  AM_DESTROY(m_vout);
  AM_DESTROY(m_iav_lock);
  AM_DESTROY(m_buffer);
  AM_DESTROY(m_stream);
  m_platform = nullptr;
  DEBUG("AMEncodeDevice is destroyed!");
}

AM_RESULT AMEncodeDevice::vin_create()
{
  AM_RESULT result = AM_RESULT_ERR_INVALID;

  AMVin *vin;
  AMVinConfigPtr vin_config = nullptr;
  AMVinParamMap vin_param;
  if (!(vin_config = AMVinConfig::get_instance())) {
    ERROR("Failed to create VIN config!");
    return result;
  }

  if ((result = vin_config->get_config(vin_param)) != AM_RESULT_OK) {
    ERROR("Failed to get VIN config!");
    return result;
  }

  for (auto &m : vin_param) {
    vin = nullptr;
    switch (m.second.type.second) {
      case AM_SENSOR_TYPE_RGB:
        vin = AMRGBSensorVin::create(m.first);
        break;
      case AM_SENSOR_TYPE_YUV:
        vin = AMYUVSensorVin::create(m.first);
        break;
      case AM_SENSOR_TYPE_YUV_GENERIC:
        vin = AMYUVGenericVin::create(m.first);
        break;
      default:
        break;
    }
    if (vin) {
      m_vin.push_back(vin);
    }
  }

  if (m_vin.size()) {
    result = AM_RESULT_OK;
  }

  return result;
}

AM_RESULT AMEncodeDevice::vin_start()
{
  AM_RESULT result = AM_RESULT_OK;
  for (auto &v : m_vin) {
    if ((result = v->start()) != AM_RESULT_OK) {
      break;
    }
  }
  return result;
}

AM_RESULT AMEncodeDevice::vin_stop()
{
  AM_RESULT result = AM_RESULT_OK;
  for (auto &v : m_vin) {
    if ((result = v->stop()) != AM_RESULT_OK) {
      break;
    }
  }
  return result;
}

AM_RESULT AMEncodeDevice::vin_delete()
{
  AM_RESULT result = AM_RESULT_OK;

  for (auto &v : m_vin) {
    AM_DESTROY(v);
  }

  return result;
}

void AMEncodeDevice::load_all_plugins()
{
  do {
    if (!m_platform) {
      ERROR("AMPlatform object is not created!");
      break;
    }

    /* WARP Plugin */
    if (m_platform->has_warp()) {
      switch(m_platform->get_dewarp_func()) {
        case AM_DEWARP_LDC:
        case AM_DEWARP_FULL: {
          AMVideoPluginMap::iterator iter =
              m_plugin_map_encode.find(VIDEO_PLUGIN_WARP);
          AMVideoPlugin *vplugin = nullptr;
          std::string warp_so(VIDEO_PLUGIN_DIR);
          warp_so.append("/").append(VIDEO_PLUGIN_WARP_SO);

          if (iter != m_plugin_map_encode.end()) {
            NOTICE("Encode Plugin \"%s\" is already loaded! Re-loading it!",
                   iter->second->plugin->name().c_str());
            delete iter->second;
            m_plugin_map_encode.erase(iter);
          }

          vplugin = load_video_plugin(warp_so.c_str(), m_vin[AM_VIN_0]);
          if (vplugin) {
            m_plugin_map_encode[VIDEO_PLUGIN_WARP] = vplugin;
            NOTICE("Encode Plugin \"%s\" is loaded!",
                   vplugin->plugin->name().c_str());
          }
        }break;
        case AM_DEWARP_EIS: {
          AMVideoPluginMap::iterator iter =
              m_plugin_map_encode.find(VIDEO_PLUGIN_EIS);
          AMVideoPlugin *vplugin = nullptr;
          std::string eis_so(VIDEO_PLUGIN_DIR);
          eis_so.append("/").append(VIDEO_PLUGIN_EIS_SO);

          if (iter != m_plugin_map_encode.end()) {
            NOTICE("Encode Plugin \"%s\" is already loaded! Re-loading it!",
                   iter->second->plugin->name().c_str());
            delete iter->second;
            m_plugin_map_encode.erase(iter);
          }

          vplugin = load_video_plugin(eis_so.c_str(), m_vin[AM_VIN_0]);
          if (vplugin) {
            m_plugin_map_encode[VIDEO_PLUGIN_EIS] = vplugin;
            NOTICE("Encode Plugin \"%s\" is loaded!",
                   vplugin->plugin->name().c_str());
          }

        }break;
        default : {
          ERROR("Invalid dewarp function %d", m_platform->get_dewarp_func());
        }break;
      }

    } else {
      NOTICE("WARP is not enabled!");
    }

    /* DPTZ Plugin */
    switch(m_platform->get_dptz()) {
      case AM_DPTZ_ENABLE: {
        AMVideoPluginMap::iterator dptz_iter =
            m_plugin_map_encode.find(VIDEO_PLUGIN_DPTZ);
        AMVideoPlugin *vplugin = nullptr;
        std::string dptz_so(VIDEO_PLUGIN_DIR);
        dptz_so.append("/").append(VIDEO_PLUGIN_DPTZ_SO);

        if (dptz_iter != m_plugin_map_encode.end()) {
          NOTICE("Encode Plugin \"%s\" is already loaded! Re-loading it!",
                 dptz_iter->second->plugin->name().c_str());
          delete dptz_iter->second;
          m_plugin_map_encode.erase(dptz_iter);
        }

        vplugin = load_video_plugin(dptz_so.c_str(), m_vin[AM_VIN_0]);
        if (vplugin) {
          m_plugin_map_encode[VIDEO_PLUGIN_DPTZ] = vplugin;
          NOTICE("Encode Plugin \"%s\" is loaded!",
                 vplugin->plugin->name().c_str());
        }
      } break;
      case AM_DPTZ_NONE: {
        NOTICE("Disable DPTZ control!");
      } break;
      default: {
        WARN("Unknown set of DPTZ");
        break;
      }
    }

    /* Bitrate Control Plugin */
    switch(m_platform->get_bitrate_ctrl_method()) {
      case AM_BITRATE_CTRL_LBR: { /* LBR */
        AMLbrPluginData lbr_data(m_vin[AM_VIN_0], m_stream);
        AMVideoPluginMap::iterator iter =
            m_plugin_map_encode.find(VIDEO_PLUGIN_LBR);
        AMVideoPlugin *vplugin = nullptr;
        std::string lbr_so(VIDEO_PLUGIN_DIR);
        lbr_so.append("/").append(VIDEO_PLUGIN_LBR_SO);

        if (iter != m_plugin_map_encode.end()) {
          NOTICE("Encode Plugin \"%s\" is already loaded! Re-loading it!",
                 iter->second->plugin->name().c_str());
          delete iter->second;
          m_plugin_map_encode.erase(iter);
        }

        vplugin = load_video_plugin(lbr_so.c_str(), &lbr_data);
        if (vplugin) {
          m_plugin_map_encode[VIDEO_PLUGIN_LBR] = vplugin;
          NOTICE("Encode Plugin \"%s\" is loaded!",
                 vplugin->plugin->name().c_str());
        }
      }break;
      /* todo: Other Bitrate Control Algorithm? */
      case AM_BITRATE_CTRL_NONE: {
        NOTICE("No special bitrate control algorithm is enabled!");
      }break;
      default: {
        WARN("Unknown bitrate control algorithm!");
      }break;
    }

    /* Overlay Plugin */
    switch(m_platform->get_overlay()) {
      case AM_OVERLAY_PLUGIN_ENABLE: {
        AMVideoPluginMap::iterator ol_iter =
            m_plugin_map_encode.find(VIDEO_PLUGIN_OVERLAY);
        AMVideoPlugin *olplugin = nullptr;
        std::string ol_so(VIDEO_PLUGIN_DIR);
        ol_so.append("/").append(VIDEO_PLUGIN_OVERLAY_SO);

        if (ol_iter != m_plugin_map_encode.end()) {
          NOTICE("Encode Plugin \"%s\" is already loaded! Re-loading it!",
                 ol_iter->second->plugin->name().c_str());
          delete ol_iter->second;
          m_plugin_map_encode.erase(ol_iter);
        }

        olplugin = load_video_plugin(ol_so.c_str(), nullptr);
        if (olplugin) {
          m_plugin_map_encode[VIDEO_PLUGIN_OVERLAY] = olplugin;
          NOTICE("Encode Plugin \"%s\" is loaded!",
                 olplugin->plugin->name().c_str());
        }
      } break;
      case AM_OVERLAY_PLUGIN_NONE: {
        NOTICE("Disable Overlay plugin!");
      } break;
      default: {
        WARN("Unknown set of Overlay");
        break;
      }
    }

  }while(0);

}

AMVideoPlugin* AMEncodeDevice::load_video_plugin(const char *name,
                                                 void *data)
{
  AMVideoPlugin *vplugin = nullptr;
  do {
    if (!name) {
      ERROR("Invalid plugin (null)!");
      break;
    }

    vplugin = new AMVideoPlugin();
    if (!vplugin) {
      ERROR("Failed to allocate memory for AMVideoPlugin object!");
      break;
    }

    vplugin->so = AMPlugin::create(name);
    if (!vplugin->so) {
      ERROR("Failed to load plugin: %s", name);
    } else {
      CreateEncodePlugin create_encode_plugin =
          (CreateEncodePlugin)vplugin->so->get_symbol(CREATE_ENCODE_PLUGIN);
      if (!create_encode_plugin) {
        ERROR("Invalid warp plugin: %s", name);
        break;
      }
      vplugin->plugin = create_encode_plugin(data);
      if (!vplugin->plugin) {
        ERROR("Failed to create object of video plugin: %s!", name);
        break;
      }
    }
  }while(0);

  if (vplugin && (!vplugin->so || !vplugin->plugin)) {
    delete vplugin;
    vplugin = nullptr;
  }

  return vplugin;
}

AM_RESULT AMEncodeDevice::start()
{
  return encode();
}

AM_RESULT AMEncodeDevice::stop()
{
  return idle();
}

AM_RESULT AMEncodeDevice::idle()
{
  return change_iav_state(AM_IAV_STATE_IDLE);
}

AM_RESULT AMEncodeDevice::preview()
{
  return change_iav_state(AM_IAV_STATE_PREVIEW);
}

AM_RESULT AMEncodeDevice::encode()
{
  return change_iav_state(AM_IAV_STATE_ENCODING);
}

AM_RESULT AMEncodeDevice::decode()
{
  return change_iav_state(AM_IAV_STATE_DECODING);
}

AM_RESULT AMEncodeDevice::get_stream_status(AM_STREAM_ID id, AM_STREAM_STATE &state)
{
  return m_platform->stream_state_get(id, state);
}

AM_RESULT AMEncodeDevice::halt_vout(AM_VOUT_ID id)
{
  return m_vout->shutdown(id);
}

AM_RESULT AMEncodeDevice::force_idr(AM_STREAM_ID stream_id)
{
  return m_platform->force_idr(stream_id);
}

AM_RESULT AMEncodeDevice::load_config()
{
  AM_RESULT result = AM_RESULT_OK;
  do {

  } while (0);
  return result;
}

AM_RESULT AMEncodeDevice::load_config_all()
{
  AM_RESULT result = AM_RESULT_OK;

  do {
    for (auto &v : m_vin) {
      if ((result = v->load_config()) != AM_RESULT_OK) {
        ERROR("Failed to load Vin[%d] config!", v->id_get());
        break;
      }
    }
    if (result != AM_RESULT_OK) {
      break;
    }

    if ((result = m_vout->load_config()) != AM_RESULT_OK) {
      break;
    }

    if ((result = m_buffer->load_config()) != AM_RESULT_OK) {
      break;
    }

    if ((result = m_stream->load_config()) != AM_RESULT_OK) {
      break;
    }

    if ((result = load_config()) != AM_RESULT_OK) {
      break;
    }

    if (result != AM_RESULT_OK) {
      break;
    }

  } while (0);
  return result;
}

AM_RESULT AMEncodeDevice::enter_preview_pre_routine()
{
  AM_RESULT result = AM_RESULT_OK;
  do {
    if ((result = vin_start()) != AM_RESULT_OK) {
      result = AM_RESULT_ERR_DSP;
      ERROR("Failed to start VIN!");
      break;
    }

    if ((result = m_vout->start()) != AM_RESULT_OK) {
      result = AM_RESULT_ERR_DSP;
      ERROR("Failed to start VOUT!");
      break;
    }

    if (set_system_resource() != AM_RESULT_OK) {
      result = AM_RESULT_ERR_DSP;
      ERROR("Failed to set system resource!");
      break;
    }

    if ((result = m_buffer->setup()) != AM_RESULT_OK) {
      result = AM_RESULT_ERR_DSP;
      ERROR("Failed to setup Buffers!");
      break;
    }
  } while (0);
  return result;
}

AM_RESULT AMEncodeDevice::enter_preview_post_routine()
{
  AM_RESULT result = AM_RESULT_OK;
  do {
    for (AMVideoPluginMap::iterator iter = m_plugin_map_preview.begin();
        iter != m_plugin_map_preview.end();
        ++ iter) {
      AMIEncodePlugin *plugin = iter->second->plugin;
      if (AM_UNLIKELY(!plugin->start())) {
        ERROR("Failed to start encode plugin %s", plugin->name().c_str());
      } else {
        INFO("Encode plugin %s started successfully!", plugin->name().c_str());
      }
    }
  } while (0);
  return result;
}

AM_RESULT AMEncodeDevice::leave_preview_pre_routine()
{
  AM_RESULT result = AM_RESULT_OK;
  do {
    if ((result = m_vout->stop()) != AM_RESULT_OK) {
      result = AM_RESULT_ERR_DSP;
      ERROR("Failed to stop VOUT!");
      break;
    }
    for (AMVideoPluginMap::iterator iter = m_plugin_map_preview.begin();
        iter != m_plugin_map_preview.end();
        ++ iter) {
      AMIEncodePlugin *plugin = iter->second->plugin;
      if (AM_UNLIKELY(!plugin->stop())) {
        ERROR("Failed to stop encode plugin %s", plugin->name().c_str());
      } else {
        INFO("Encode plugin %s stopped successfully!", plugin->name().c_str());
      }
    }
  } while (0);

  return result;
}

AM_RESULT AMEncodeDevice::enter_encode_pre_routine()
{
  AM_RESULT result = AM_RESULT_OK;
  do {
    if ((result = m_stream->setup()) != AM_RESULT_OK) {
      break;
    }
  } while (0);
  return result;
}

AM_RESULT AMEncodeDevice::enter_encode_post_routine()
{
  AM_RESULT result = AM_RESULT_OK;
  do {
    for (AMVideoPluginMap::iterator iter = m_plugin_map_encode.begin();
        iter != m_plugin_map_encode.end();
        ++ iter) {
      AMIEncodePlugin *plugin = iter->second->plugin;
      if (AM_UNLIKELY(!plugin->start())) {
        ERROR("Failed to start encode plugin %s", plugin->name().c_str());
      } else {
        INFO("Encode plugin %s started successfully!", plugin->name().c_str());
      }
    }
  } while (0);
  return result;
}

AM_RESULT AMEncodeDevice::leave_encode_pre_routine()
{
  AM_RESULT result = AM_RESULT_OK;
  do {
    for (AMVideoPluginMap::iterator iter = m_plugin_map_encode.begin();
        iter != m_plugin_map_encode.end();
        ++ iter) {
      AMIEncodePlugin *plugin = iter->second->plugin;
      if (AM_UNLIKELY(!plugin->stop())) {
        ERROR("Failed to stop encode plugin %s", plugin->name().c_str());
      } else {
        INFO("Encode plugin %s stopped successfully!", plugin->name().c_str());
      }
    }
  } while (0);
  return result;
}

AM_RESULT AMEncodeDevice::leave_encode_post_routine()
{
  AM_RESULT result = AM_RESULT_OK;
  do {

  } while (0);
  return result;
}

AM_RESULT AMEncodeDevice::enter_decode_routine()
{
  AM_RESULT result = AM_RESULT_OK;
  do {

  } while (0);
  return result;
}

AM_RESULT AMEncodeDevice::leave_decode_routine()
{
  AM_RESULT result = AM_RESULT_OK;
  do {

  } while (0);
  return result;
}

AM_RESULT AMEncodeDevice::idle_routine_from_decode()
{
  AM_RESULT result = AM_RESULT_OK;
  do {

  } while (0);
  return result;
}

AM_RESULT AMEncodeDevice::leave_preview_post_routine()
{
  AM_RESULT result = AM_RESULT_OK;
  do {
    if ((result = vin_stop()) != AM_RESULT_OK) {
      result = AM_RESULT_ERR_DSP;
      ERROR("Failed to stop VIN!");
      break;
    }
  } while (0);
  return result;
}

AM_RESULT AMEncodeDevice::goto_idle()
{
  return m_platform->goto_idle();
}

AM_RESULT AMEncodeDevice::enter_preview()
{
  return m_platform->goto_preview();
}

AM_RESULT AMEncodeDevice::start_encode()
{
  return m_stream->start_encode();
}

AM_RESULT AMEncodeDevice::stop_encode()
{
  return m_stream->stop_encode();
}

AM_RESULT AMEncodeDevice::start_decode()
{
 return m_platform->decode_start();
}

AM_RESULT AMEncodeDevice::stop_decode()
{
  return m_platform->decode_stop();
}

AM_IAV_STATE AMEncodeDevice::get_iav_state()
{
  AM_IAV_STATE state = AM_IAV_STATE_ERROR;
  if (m_platform->iav_state_get(state) != AM_RESULT_OK) {
    state = AM_IAV_STATE_ERROR;
  }
  return state;
}

AM_RESULT AMEncodeDevice::change_iav_state(AM_IAV_STATE target)
{
  AUTO_LOCK(m_iav_lock);
  const uint32_t MAX_TRY_TIMES = 5;
  AM_RESULT result = AM_RESULT_OK;
  std::pair<AM_IAV_STATE, uint32_t> last_state = {AM_IAV_STATE_INIT, 0};

  if (target == AM_IAV_STATE_INIT ||
      target == AM_IAV_STATE_EXITING_PREVIEW ||
      target == AM_IAV_STATE_ERROR) {
    ERROR("Invalid IAV state: %d!", target);
    return result;
  }

  while (m_state = get_iav_state(), true) {
    PRINTF("IAV state: %s %s %s!",
           AMVideoTrans::iav_state_to_str(m_state).c_str(),
           (m_state == target) ? "==" : "-->",
           AMVideoTrans::iav_state_to_str(target).c_str());

    if (m_state == target) {
      break;
    }

    if ((m_state == last_state.first) &&
        (++last_state.second >= MAX_TRY_TIMES)) {
      result = AM_RESULT_ERR_DSP;
      ERROR("Failed to change IAV state!");
      break;
    }

    switch (last_state.first = m_state) {
      case AM_IAV_STATE_INIT: {
        result = goto_idle();
      } break;

      case AM_IAV_STATE_IDLE: {
        switch (target) {
          case AM_IAV_STATE_PREVIEW:
          case AM_IAV_STATE_ENCODING:
            if ((result = enter_preview_pre_routine()) != AM_RESULT_OK) {
              break;
            }
            if ((result = enter_preview()) != AM_RESULT_OK) {
              ERROR("Failed to enter preview!");
              break;
            }
            if ((result = enter_preview_post_routine()) != AM_RESULT_OK) {
              break;
            }
            break;
          case AM_IAV_STATE_DECODING:
            if ((result = enter_decode_routine()) != AM_RESULT_OK) {
              break;
            }
            if ((result = start_decode()) != AM_RESULT_OK) {
              ERROR("Failed to start decode!");
              break;
            }
            break;
          default:
            break;
        }
      } break;

      case AM_IAV_STATE_PREVIEW: {
        switch (target) {
          case AM_IAV_STATE_ENCODING:
            if ((result = enter_encode_pre_routine()) != AM_RESULT_OK) {
              break;
            }
            if ((result = start_encode()) != AM_RESULT_OK) {
              ERROR("Failed to start encode!");
              if (result == AM_RESULT_ERR_PERM) {
                WARN("Enter preview state!");
                target = AM_IAV_STATE_PREVIEW;
                result = AM_RESULT_OK;
              }
              break;
            }
            if ((result = enter_encode_post_routine()) != AM_RESULT_OK) {
              break;
            }
            break;
          case AM_IAV_STATE_IDLE:
            if ((result = leave_preview_pre_routine()) != AM_RESULT_OK) {
              break;
            }
            if ((result = goto_idle()) != AM_RESULT_OK) {
              ERROR("Failed to goto idle!");
              break;
            }
            if ((result = leave_preview_post_routine()) != AM_RESULT_OK) {
              break;
            }
            break;
          case AM_IAV_STATE_DECODING:
            if ((result = goto_idle()) != AM_RESULT_OK) {
              ERROR("Failed to goto idle!");
              break;
            }
            if ((result = idle_routine_from_decode()) != AM_RESULT_OK) {
              break;
            }
            break;
          default:
            break;
        }
      } break;

      case AM_IAV_STATE_ENCODING: {
        if ((result = leave_encode_pre_routine()) != AM_RESULT_OK) {
          break;
        }
        if ((result = stop_encode()) != AM_RESULT_OK) {
          break;
        }
        if ((result = leave_encode_post_routine()) != AM_RESULT_OK) {
          break;
        }
      } break;

      case AM_IAV_STATE_EXITING_PREVIEW: {
        usleep(100000);
      } break;

      case AM_IAV_STATE_DECODING: {
        if ((result = leave_decode_routine()) == AM_RESULT_OK) {
          result = stop_decode();
        }
      } break;

      case AM_IAV_STATE_ERROR: {
      } break;

      default: {
      } break;
    }
    if (result != AM_RESULT_OK) {
      break;
    }
  }

  return result;
}

AM_RESULT AMEncodeDevice::set_system_resource()
{
  AM_RESULT result = AM_RESULT_OK;

  do {
    AMStreamParamMap stream_param;
     if ((result = m_stream->get_param(stream_param)) != AM_RESULT_OK) {
       ERROR("Failed to get stream param!");
       break;
     }

     if ((result = m_platform->system_resource_set(stream_param)) !=
         AM_RESULT_OK) {
       ERROR("Failed to set system resource limit!");
     }
  } while (0);

  return result;
}

void* AMEncodeDevice::get_video_plugin_interface(const std::string &plugin_name)
{
  AMVideoPluginMap::iterator iter_prev = m_plugin_map_preview.find(plugin_name);
  AMVideoPluginMap::iterator iter_enc = m_plugin_map_encode.find(plugin_name);
  return ((iter_enc != m_plugin_map_encode.end()) ?
      (iter_enc->second->plugin->get_interface()) :
      ((iter_prev != m_plugin_map_preview.end()) ?
          iter_prev->second->plugin->get_interface() :
          nullptr));
}

AM_RESULT AMEncodeDevice::process_parameters()
{
  AM_RESULT result = AM_RESULT_OK;

  do {
    AMVinParamMap vin_param;
    AMBufferParamMap buffer_param;
    AMStreamParamMap stream_param;
    for (auto &m : m_vin) {
      m->get_param(vin_param);
      break;
    }
    m_buffer->get_param(buffer_param);
    m_stream->get_param(stream_param);

    //TODO: process the parameters of all moudles
    //    if ((result = do_something()) != AM_RESULT_OK) {
    //      ERROR("Failed to ...");
    //      break;
    //    }

    for (auto &v : m_vin) {
      v->set_param(vin_param);
    }
    m_buffer->set_param(buffer_param);
    m_stream->set_param(stream_param);
  } while (0);

  return result;
}
