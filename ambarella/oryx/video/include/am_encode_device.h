/**
 * am_encode_device.h
 *
 *  History:
 *    Jul 10, 2015 - [Shupeng Ren] created file
 *
 * Copyright (c) 2016 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ORYX_VIDEO_INCLUDE_AM_ENCODE_DEVICE_H_
#define ORYX_VIDEO_INCLUDE_AM_ENCODE_DEVICE_H_

#include "am_platform_if.h"

using std::vector;
using std::map;

class AMVin;
class AMVout;
class AMMutex;
class AMPlugin;
class AMEncodeBuffer;
class AMEncodeStream;
class AMEncodeOverlay;
class AMIEncodeWarp;
class AMIEncodePlugin;

struct AMVideoPlugin
{
    AMPlugin        *so;
    AMIEncodePlugin *plugin;
    AMVideoPlugin();
    AMVideoPlugin(const AMVideoPlugin &vplugin);
    ~AMVideoPlugin();
};
typedef std::map<std::string, AMVideoPlugin*> AMVideoPluginMap;

class AMEncodeDevice
{
  public:
    static AMEncodeDevice *create();
    virtual void destroy();

    virtual AM_RESULT start();
    virtual AM_RESULT stop();

    virtual AM_RESULT idle();
    virtual AM_RESULT preview();
    virtual AM_RESULT encode();
    virtual AM_RESULT decode();

    virtual AM_RESULT get_stream_status(AM_STREAM_ID id,
                                        AM_STREAM_STATE &state);

    virtual AM_RESULT halt_vout(AM_VOUT_ID id);

    virtual AM_RESULT force_idr(AM_STREAM_ID stream_id);

    /* For video Plugin */
    virtual void* get_video_plugin_interface(const std::string &plugin_name);

    virtual AM_RESULT load_config();
    virtual AM_RESULT load_config_all();

  private:
    virtual AM_RESULT goto_idle();
    virtual AM_RESULT enter_preview();
    virtual AM_RESULT start_encode();
    virtual AM_RESULT stop_encode();
    virtual AM_RESULT start_decode();
    virtual AM_RESULT stop_decode();

    virtual AM_RESULT change_iav_state(AM_IAV_STATE state);
    virtual AM_IAV_STATE get_iav_state();

    virtual AM_RESULT enter_preview_pre_routine();
    virtual AM_RESULT enter_preview_post_routine();
    virtual AM_RESULT enter_encode_pre_routine();
    virtual AM_RESULT enter_encode_post_routine();
    virtual AM_RESULT leave_encode_pre_routine();
    virtual AM_RESULT leave_encode_post_routine();
    virtual AM_RESULT leave_preview_pre_routine();
    virtual AM_RESULT leave_preview_post_routine();

    virtual AM_RESULT enter_decode_routine();
    virtual AM_RESULT leave_decode_routine();
    virtual AM_RESULT idle_routine_from_decode();

    virtual AM_RESULT set_system_resource();
    virtual AM_RESULT process_parameters();

  private:
    AM_RESULT vin_create();
    AM_RESULT vin_start();
    AM_RESULT vin_stop();
    AM_RESULT vin_delete();
    void load_all_plugins();
    AMVideoPlugin* load_video_plugin(const char *name, void *data);

  private:
    AMEncodeDevice();
    virtual ~AMEncodeDevice();
    AM_RESULT init();

  private:
    AMEncodeBuffer       *m_buffer;
    AMEncodeStream       *m_stream;
    AMVout               *m_vout;
    AMMutex              *m_iav_lock;
    AMIPlatformPtr        m_platform;
    AM_IAV_STATE          m_state;
    vector<AMVin*>        m_vin;
    AMVideoPluginMap      m_plugin_map_encode;
    AMVideoPluginMap      m_plugin_map_preview;
};

#endif /* ORYX_VIDEO_INCLUDE_AM_ENCODE_DEVICE_H_ */
