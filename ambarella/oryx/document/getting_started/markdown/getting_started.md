![Oryx logo](img/Oryx-Logo.jpg) 
#Oryx getting started guide

## How to use?

### Setup PC
### Setup board
### Watch live stream
### Apply different config sets


## Oryx modules
### Video control
####....

### Stream engine
#### ....

## Oryx services
### Audio service
### Video service
### Media service
### Image service

#Markdown simple guide

## Introducing Markdown

> Markdown is a plain text formatting syntax designed to be converted to HTML. Markdown is popularly used as format for readme files, ... or in text editors for the quick creation of rich text documents.  - [Wikipedia](http://en.wikipedia.org/wiki/Markdown)

### Styling text
#### Bold
**This is a bold text line!**

#### Italic
*This is a italic text line!*

### Code block

#### C++
``` c++
/* This is sample C++ code */
#include <cstdio>
#define MACRO(x) x
using namespace std;
// This comment may span only this line
typedef unsigned int uint;
int static myfunc(uint parameter) {
  if (parameter == 0) fprintf(stdout, "zero\n");
  cout << "hello\n";
  return parameter - 1;
}
class MyClass {
public:
  enum Number { ZERO, ONE, TWO };
  static char staticField;
  int field;
  virtual Number vmethod();
  void method(Number n) const {
    int local= (int)MACRO('\0');
label: myfunc(local);
    vmethod();
    staticMethod();
    problem();
  }
  static void staticMethod();
};
```

#### Javascript
``` javascript
/**
 * This is about <code>Example.js</code>.
 * {@link com.yourCompany.aPackage.aFile}
 * @author author
 * @deprecated use <code>BetterExample.js</code>
 */
var index = 0;
var arr = [];

function foo() {
    /* This comment may span multiple lines. */
    var var1 = new Object();
    var obj = { carMake: 'Amet', carModel: 'Porro', carYear: 2012 };
    var s = "abc123";
    clear(var1);
}

function pop() {
    return arr[index--];
}

function push(elem) {
    // This comment may span only this line
    arr[index++] = elem;
}

function isEmpty() {
    // TASK: refactor
    return index == 0;
}
```

#### Pyton
``` python
@requires_authorization
def somefunc(param1='', param2=0):
    '''A docstring'''
    if param1 > param2: # interesting
        print 'Greater'
    return (param2 - param1 + 1) or None
class SomeClass:
    pass
>>> message = '''interpreter
... prompt'''
```

#### Lua
``` lua
---
-- Description of the module.
-- @module moduleName
local M = {
  -- single-line comment
  field = 1
}
-- TODO Task in the comment
function M.sample(...)
  --[[
  Long comment
  ]]
  local table = {
    foo = 'bar',
    42
  }
  for index=1,select("#", ...) do
    local var = select(index, ...)
  end
end
return M
```

### Table
| Item      |    Value | Qty  |
| :-------- | --------:| :--: |
| Computer  | 1600 USD |  5   |
| Phone     |   12 USD |  12  |
| Pipe      |    1 USD | 234  |

### Lists
#### Checkbox
You can use `- [ ]` and `- [x]` to create checkboxes, for example:

- [x] Item1
- [ ] Item2
- [ ] Item3

#### Unordered lists
- list1
- list2
- list3

#### Ordered lists
1. list1
2. list2
3. list3

#### Nested lists
You can create nested lists by indenting list items by two spaces.
```
    1. Item 1
      1. A corollary to the above item.
      2. Yet another point to consider.
    2. Item 2
      * A corollary that does not need to be ordered.
      * This is indented four spaces, because it's two spaces further than the item above.
      * You might want to consider making a new list.
    3. Item 3
```
