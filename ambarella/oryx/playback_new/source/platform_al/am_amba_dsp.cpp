/**
 * am_amba_dsp.cpp
 *
 * History:
 *    2015/07/31 - [Zhi He] create file
 *
 * Copyright (c) 2015 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "am_native.h"
#include "am_native_log.h"

#include "am_internal.h"
#include "am_amba_dsp.h"

#ifdef BUILD_MODULE_AMBA_DSP

#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#ifdef BUILD_DSP_AMBA_S2L

#include "errno.h"
#include "basetypes.h"
#include "iav_ioctl.h"

static TInt __s2l_enter_decode_mode(TInt iav_fd, SAmbaDSPDecodeModeConfig *mode_config)
{
  struct iav_decode_mode_config decode_mode;
  TInt i = 0;
  memset(&decode_mode, 0x0, sizeof(decode_mode));
  if (mode_config->num_decoder > DAMBADSP_MAX_DECODER_NUMBER) {
    LOG_FATAL("BAD num_decoder %d\n", mode_config->num_decoder);
    return (-100);
  }
  decode_mode.num_decoder = mode_config->num_decoder;
  for (i = 0; i < decode_mode.num_decoder; i ++) {
    decode_mode.decoder_configs[i].max_frm_num = mode_config->decoder_configs[i].max_frm_num;
    decode_mode.decoder_configs[i].max_frm_width = mode_config->decoder_configs[i].max_frm_width;
    decode_mode.decoder_configs[i].max_frm_height = mode_config->decoder_configs[i].max_frm_height;
    decode_mode.decoder_configs[i].b_support_ff = mode_config->decoder_configs[i].b_support_ff;
    decode_mode.decoder_configs[i].b_support_fb = mode_config->decoder_configs[i].b_support_fb;
    decode_mode.decoder_configs[i].b_support_bw = mode_config->decoder_configs[i].b_support_bw;
  }
  i = ioctl(iav_fd, IAV_IOC_ENTER_DECODE_MODE, &decode_mode);
  if (0 > i) {
    perror("IAV_IOC_ENTER_UDEC_MODE");
    LOG_FATAL("enter decode mode fail, errno %d\n", errno);
    return i;
  }
  return 0;
}

static TInt __s2l_leave_decode_mode(TInt iav_fd)
{
  TInt ret = ioctl(iav_fd, IAV_IOC_LEAVE_DECODE_MODE);
  if (0 > ret) {
    perror("IAV_IOC_LEAVE_DECODE_MODE");
    LOG_FATAL("leave decode mode fail, errno %d\n", errno);
  }
  return ret;
}

static TInt __s2l_create_decoder(TInt iav_fd, SAmbaDSPDecoderInfo *p_decoder_info)
{
  TInt i = 0;
  struct iav_decoder_info decoder_info;
  memset(&decoder_info, 0x0, sizeof(decoder_info));
  decoder_info.decoder_id = p_decoder_info->decoder_id;
  if (EAMDSP_VIDEO_CODEC_TYPE_H264 == p_decoder_info->decoder_type) {
    decoder_info.decoder_type = IAV_DECODER_TYPE_H264;
  } else if (EAMDSP_VIDEO_CODEC_TYPE_H265 == p_decoder_info->decoder_type) {
    decoder_info.decoder_type = IAV_DECODER_TYPE_H265;
  } else {
    LOG_FATAL("bad video codec type %d\n", p_decoder_info->decoder_type);
    return (-101);
  }
  decoder_info.num_vout = p_decoder_info->num_vout;
  decoder_info.setup_done = p_decoder_info->setup_done;
  decoder_info.width = p_decoder_info->width;
  decoder_info.height = p_decoder_info->height;
  if (decoder_info.num_vout > DAMBADSP_MAX_DECODE_VOUT_NUMBER) {
    LOG_FATAL("BAD num_vout %d\n", p_decoder_info->num_vout);
    return (-100);
  }
  for (i = 0; i < decoder_info.num_vout; i ++) {
    decoder_info.vout_configs[i].vout_id = p_decoder_info->vout_configs[i].vout_id;
    decoder_info.vout_configs[i].enable = p_decoder_info->vout_configs[i].enable;
    decoder_info.vout_configs[i].flip = p_decoder_info->vout_configs[i].flip;
    decoder_info.vout_configs[i].rotate = p_decoder_info->vout_configs[i].rotate;
    decoder_info.vout_configs[i].target_win_offset_x = p_decoder_info->vout_configs[i].target_win_offset_x;
    decoder_info.vout_configs[i].target_win_offset_y = p_decoder_info->vout_configs[i].target_win_offset_y;
    decoder_info.vout_configs[i].target_win_width = p_decoder_info->vout_configs[i].target_win_width;
    decoder_info.vout_configs[i].target_win_height = p_decoder_info->vout_configs[i].target_win_height;
    decoder_info.vout_configs[i].zoom_factor_x = p_decoder_info->vout_configs[i].zoom_factor_x;
    decoder_info.vout_configs[i].zoom_factor_y = p_decoder_info->vout_configs[i].zoom_factor_y;
    decoder_info.vout_configs[i].vout_mode = p_decoder_info->vout_configs[i].vout_mode;
    LOG_PRINTF("vout(%d), w %d, h %d, zoom x %08x, y %08x\n", i, decoder_info.vout_configs[i].target_win_width, decoder_info.vout_configs[i].target_win_height, decoder_info.vout_configs[i].zoom_factor_x, decoder_info.vout_configs[i].zoom_factor_y);
  }
  decoder_info.bsb_start_offset = p_decoder_info->bsb_start_offset;
  decoder_info.bsb_size = p_decoder_info->bsb_size;
  i = ioctl(iav_fd, IAV_IOC_CREATE_DECODER, &decoder_info);
  if (0 > i) {
    perror("IAV_IOC_CREATE_DECODER");
    LOG_FATAL("create decoder fail, errno %d\n", errno);
    return i;
  }
  p_decoder_info->bsb_start_offset = decoder_info.bsb_start_offset;
  p_decoder_info->bsb_size = decoder_info.bsb_size;
  return 0;
}

static TInt __s2l_destroy_decoder(TInt iav_fd, TU8 decoder_id)
{
  TInt ret = ioctl(iav_fd, IAV_IOC_DESTROY_DECODER, decoder_id);
  if (0 > ret) {
    perror("IAV_IOC_DESTROY_DECODER");
    LOG_FATAL("destroy decoder fail, errno %d\n", errno);
  }
  return ret;
}

static int __s2l_decode_trick_play(int iav_fd, TU8 decoder_id, TU8 trick_play)
{
  int ret;
  struct iav_decode_trick_play trickplay;
  trickplay.decoder_id = decoder_id;
  trickplay.trick_play = trick_play;
  ret = ioctl(iav_fd, IAV_IOC_DECODE_TRICK_PLAY, &trickplay);
  if (0 > ret) {
    perror("IAV_IOC_DECODE_TRICK_PLAY");
    LOG_ERROR("trickplay error, errno %d\n", errno);
    return ret;
  }
  return 0;
}

static int __s2l_decode_start(int iav_fd, TU8 decoder_id)
{
  int ret = ioctl(iav_fd, IAV_IOC_DECODE_START, decoder_id);
  if (ret < 0) {
    perror("IAV_IOC_DECODE_START");
    LOG_ERROR("decode start error, errno %d\n", errno);
    return ret;
  }
  return 0;
}

static int __s2l_decode_stop(int iav_fd, TU8 decoder_id, TU8 stop_flag)
{
  int ret;
  struct iav_decode_stop stop;
  stop.decoder_id = decoder_id;
  stop.stop_flag = stop_flag;
  ret = ioctl(iav_fd, IAV_IOC_DECODE_STOP, &stop);
  if (0 > ret) {
    perror("IAV_IOC_UDEC_STOP");
    LOG_ERROR("decode stop error, errno %d\n", errno);
    return ret;
  }
  return 0;
}

static int __s2l_decode_speed(int iav_fd, TU8 decoder_id, TU16 speed, TU8 scan_mode, TU8 direction)
{
  int ret;
  struct iav_decode_speed spd;
  spd.decoder_id = decoder_id;
  spd.direction = direction;
  spd.speed = speed;
  spd.scan_mode = scan_mode;
  LOG_PRINTF("speed, direction %d, speed %x, scanmode %d\n", spd.direction, spd.speed, spd.scan_mode);
  ret = ioctl(iav_fd, IAV_IOC_DECODE_SPEED, &spd);
  if (0 > ret) {
    perror("IAV_IOC_DECODE_SPEED");
    LOG_ERROR("decode speed error, errno %d\n", errno);
    return ret;
  }
  return 0;
}

static int __s2l_decode_request_bits_fifo(int iav_fd, int decoder_id, TU32 size, void *cur_pos_offset)
{
  struct iav_decode_bsb wait;
  int ret;
  wait.decoder_id = decoder_id;
  wait.room = size;
  wait.start_offset = (TU32) cur_pos_offset;
  ret = ioctl(iav_fd, IAV_IOC_WAIT_DECODE_BSB, &wait);
  if (0 > ret) {
    LOG_ERROR("IAV_IOC_WAIT_DECODE_BSB fail, errno %d.\n", errno);
    perror("IAV_IOC_WAIT_DECODE_BSB");
    return ret;
  }
  return 0;
}

static int __s2l_decode(int iav_fd, SAmbaDSPDecode *dec)
{
  TInt ret = 0;
  struct iav_decode_video decode_video;
  memset(&decode_video, 0, sizeof(decode_video));
  decode_video.decoder_id = dec->decoder_id;
  decode_video.num_frames = dec->num_frames;
  decode_video.start_ptr_offset = dec->start_ptr_offset;
  decode_video.end_ptr_offset = dec->end_ptr_offset;
  decode_video.first_frame_display = dec->first_frame_display;
  ret = ioctl(iav_fd, IAV_IOC_DECODE_VIDEO, &decode_video);
  if (0 > ret) {
    perror("IAV_IOC_DECODE_VIDEO");
    LOG_ERROR("IAV_IOC_DECODE_VIDEO fail, errno %d.\n", errno);
    return ret;
  }
  return 0;
}

static int __s2l_decode_query_bsb_status_and_print(int iav_fd, TU8 decoder_id)
{
  int ret;
  struct iav_decode_bsb bsb;
  memset(&bsb, 0x0, sizeof(bsb));
  bsb.decoder_id = decoder_id;
  ret = ioctl(iav_fd, IAV_IOC_QUERY_DECODE_BSB, &bsb);
  if (0 > ret) {
    LOG_ERROR("IAV_IOC_QUERY_DECODE_BSB fail, errno %d.\n", errno);
    perror("IAV_IOC_QUERY_DECODE_BSB");
    return ret;
  }
  LOG_PRINTF("[bsb]: current write offset (arm) 0x%08x, current read offset (dsp) 0x%08x, safe room (minus 256 bytes) %d, free room %d\n", bsb.start_offset, bsb.dsp_read_offset, bsb.room, bsb.free_room);
  return 0;
}

static int __s2l_decode_query_status_and_print(int iav_fd, TU8 decoder_id)
{
  int ret;
  struct iav_decode_status status;
  memset(&status, 0x0, sizeof(status));
  status.decoder_id = decoder_id;
  ret = ioctl(iav_fd, IAV_IOC_QUERY_DECODE_STATUS, &status);
  if (0 > ret) {
    perror("IAV_IOC_QUERY_DECODE_STATUS");
    LOG_ERROR("IAV_IOC_QUERY_DECODE_STATUS fail, errno %d.\n", errno);
    return ret;
  }
  LOG_PRINTF("[decode status]: decode_state %d, decoded_pic_number %d, error_status %d, total_error_count %d, irq_count %d\n", status.decode_state, status.decoded_pic_number, status.error_status, status.total_error_count, status.irq_count);
  LOG_PRINTF("[decode status, bsb]: current write offset (arm) 0x%08x, current read offset (dsp) 0x%08x, safe room (minus 256 bytes) %d, free room %d\n", status.write_offset, status.dsp_read_offset, status.room, status.free_room);
  LOG_PRINTF("[decode status, last pts]: %d, is_started %d, is_send_stop_cmd %d\n", status.last_pts, status.is_started, status.is_send_stop_cmd);
  LOG_PRINTF("[decode status, yuv addr]: yuv422_y 0x%08x, yuv422_uv 0x%08x\n", status.yuv422_y_addr, status.yuv422_uv_addr);
  return 0;
}

static int __s2l_decode_query_bsb_status(int iav_fd, SAmbaDSPBSBStatus *status)
{
  int ret;
  struct iav_decode_bsb bsb;
  memset(&bsb, 0x0, sizeof(bsb));
  bsb.decoder_id = status->decoder_id;
  ret = ioctl(iav_fd, IAV_IOC_QUERY_DECODE_BSB, &bsb);
  if (0 > ret) {
    LOG_ERROR("IAV_IOC_QUERY_DECODE_BSB fail, errno %d.\n", errno);
    perror("IAV_IOC_QUERY_DECODE_BSB");
    return ret;
  }
  status->start_offset = bsb.start_offset;
  status->room = bsb.room;
  status->dsp_read_offset = bsb.dsp_read_offset;
  status->free_room = bsb.free_room;
  return 0;
}

static int __s2l_decode_query_status(int iav_fd, SAmbaDSPDecodeStatus *status)
{
  int ret;
  struct iav_decode_status dec_status;
  memset(&dec_status, 0x0, sizeof(dec_status));
  dec_status.decoder_id = status->decoder_id;
  ret = ioctl(iav_fd, IAV_IOC_QUERY_DECODE_STATUS, &dec_status);
  if (0 > ret) {
    perror("IAV_IOC_QUERY_DECODE_STATUS");
    LOG_ERROR("IAV_IOC_QUERY_DECODE_STATUS fail, errno %d.\n", errno);
    return ret;
  }
  status->is_started = dec_status.is_started;
  status->is_send_stop_cmd = dec_status.is_send_stop_cmd;
  status->last_pts = dec_status.last_pts;
  status->decode_state = dec_status.decode_state;
  status->error_status = dec_status.error_status;
  status->total_error_count = dec_status.total_error_count;
  status->decoded_pic_number = dec_status.decoded_pic_number;
  status->write_offset = dec_status.write_offset;
  status->room = dec_status.room;
  status->dsp_read_offset = dec_status.dsp_read_offset;
  status->free_room = dec_status.free_room;
  status->irq_count = dec_status.irq_count;
  status->yuv422_y_addr = dec_status.yuv422_y_addr;
  status->yuv422_uv_addr = dec_status.yuv422_uv_addr;
  return 0;
}

static int __s2l_get_single_vout_info(int iav_fd, int chan, int type, SAmbaDSPVoutInfo *voutinfo)
{
  int num;
  int sink_type = 0;
  int i;
  struct amba_vout_sink_info  sink_info;
  if (EAMDSP_VOUT_TYPE_DIGITAL == type) {
    sink_type = AMBA_VOUT_SINK_TYPE_DIGITAL;
  } else if (EAMDSP_VOUT_TYPE_HDMI == type) {
    sink_type = AMBA_VOUT_SINK_TYPE_HDMI;
  } else if (EAMDSP_VOUT_TYPE_CVBS == type) {
    sink_type = AMBA_VOUT_SINK_TYPE_CVBS;
  } else {
    LOG_ERROR("not valid type %d\n", type);
    return (-1);
  }
  num = 0;
  if (ioctl(iav_fd, IAV_IOC_VOUT_GET_SINK_NUM, &num) < 0) {
    perror("IAV_IOC_VOUT_GET_SINK_NUM");
    LOG_ERROR("IAV_IOC_VOUT_GET_SINK_NUM fail, errno %d\n", errno);
    return (-2);
  }
  if (num < 1) {
    LOG_PRINTF("Please load vout driver!\n");
    return (-3);
  }
  for (i = 0; i < num; i++) {
    sink_info.id = i;
    if (ioctl(iav_fd, IAV_IOC_VOUT_GET_SINK_INFO, &sink_info) < 0) {
      perror("IAV_IOC_VOUT_GET_SINK_INFO");
      LOG_ERROR("IAV_IOC_VOUT_GET_SINK_NUM fail, errno %d\n", errno);
      return (-4);
    }
    if ((sink_info.state == AMBA_VOUT_SINK_STATE_RUNNING) && (sink_info.sink_type == sink_type) && (sink_info.source_id == chan)) {
      if (voutinfo) {
        voutinfo->sink_id = sink_info.id;
        voutinfo->source_id = sink_info.source_id;
        voutinfo->sink_type = sink_info.sink_type;
        voutinfo->width = sink_info.sink_mode.video_size.vout_width;
        voutinfo->height = sink_info.sink_mode.video_size.vout_height;
        voutinfo->offset_x = sink_info.sink_mode.video_offset.offset_x;
        voutinfo->offset_y = sink_info.sink_mode.video_offset.offset_y;
        voutinfo->rotate = sink_info.sink_mode.video_rotate;
        voutinfo->flip = sink_info.sink_mode.video_flip;
        voutinfo->mode =  sink_info.sink_mode.mode;
        return 0;
      }
    }
  }
  LOG_NOTICE("no vout with type %d, id %d\n", type, chan);
  return (-5);
}

#if 0
static void __parse_fps(TU32 fps_q9, SAmbaDSPVinInfo *vininfo)
{
  switch (fps_q9) {
    case AMBA_VIDEO_FPS_AUTO:
      vininfo->fps = 30;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3003;
      LOG_CONFIGURATION("auto fps, set 29.97 as default\n");
      break;
    case AMBA_VIDEO_FPS_29_97:
      vininfo->fps = 30;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3003;
      break;
    case AMBA_VIDEO_FPS_30:
      vininfo->fps = 30;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3000;
      break;
    case AMBA_VIDEO_FPS_59_94:
      vininfo->fps = 60;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 1501;
      break;
    case AMBA_VIDEO_FPS_60:
      vininfo->fps = 60;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 1500;
      break;
    case AMBA_VIDEO_FPS_120:
      vininfo->fps = 120;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 750;
      break;
    case AMBA_VIDEO_FPS_23_976:
      vininfo->fps = 24;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3754;
      break;
    case AMBA_VIDEO_FPS_24:
      vininfo->fps = 24;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3750;
      break;
    case AMBA_VIDEO_FPS_25:
      vininfo->fps = 25;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3600;
      break;
    case AMBA_VIDEO_FPS_20:
      vininfo->fps = 20;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 4500;
      break;
    case AMBA_VIDEO_FPS_15:
      vininfo->fps = 15;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 6000;
      break;
    case AMBA_VIDEO_FPS_12:
      vininfo->fps = 12;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 7500;
      break;
    case AMBA_VIDEO_FPS_10:
      vininfo->fps = 10;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 9000;
      break;
    case AMBA_VIDEO_FPS_6:
      vininfo->fps = 6;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 15000;
      break;
    case AMBA_VIDEO_FPS_5:
      vininfo->fps = 5;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 18000;
      break;
    case AMBA_VIDEO_FPS_4:
      vininfo->fps = 4;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 22500;
      break;
    case AMBA_VIDEO_FPS_3:
      vininfo->fps = 3;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 30000;
      break;
    case AMBA_VIDEO_FPS_2:
      vininfo->fps = 2;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 45000;
      break;
    case AMBA_VIDEO_FPS_1:
      vininfo->fps = 1;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 90000;
      break;
    default:
      vininfo->fps = (TU64)((TU64) 512000000 + (fps_q9 >> 1)) / (TU64)(fps_q9);
      vininfo->fr_num = 90000;
      vininfo->fr_den = (float) 90000 * (float)(fps_q9) / ((float)((TU64) 512000000 + (TU64)(fps_q9 >> 1)));
      break;
  }
  return;
}

#else

static void __parse_fps(u32 fps_q9, SAmbaDSPVinInfo *vininfo)
{
  switch (fps_q9) {
    case AMBA_VIDEO_FPS_AUTO:
      vininfo->fps = 30;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3003;
      break;
    case AMBA_VIDEO_FPS_29_97:
      vininfo->fps = 30;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3003;
      break;
    case AMBA_VIDEO_FPS_59_94:
      vininfo->fps = 60;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 1501;
      break;
    default:
      vininfo->fps = (TU64)((TU64) 512000000 + (fps_q9 >> 1)) / (TU64)(fps_q9);
      vininfo->fr_num = 90000;
      vininfo->fr_den = (TU64) 90000 * (TU64)(fps_q9) / (512000000);
      break;
  }
}

#endif

static int __s2l_get_vin_info(int iav_fd, SAmbaDSPVinInfo *vininfo)
{
  struct vindev_video_info vin_info;
  struct vindev_fps active_fps;
  TU32 fps_q9 = 1;
  int ret = 0;
  memset(&vin_info, 0x0, sizeof(vin_info));
  vin_info.vsrc_id = 0;
  vin_info.info.mode = AMBA_VIDEO_MODE_CURRENT;
  ret = ioctl(iav_fd, IAV_IOC_VIN_GET_VIDEOINFO, &vin_info);
  if (0 > ret) {
    perror("IAV_IOC_VIN_GET_VIDEOINFO");
    LOG_WARN("IAV_IOC_VIN_GET_VIDEOINFO fail, errno %d\n", errno);
    return ret;
  }
  memset(&active_fps, 0, sizeof(active_fps));
  active_fps.vsrc_id = 0;
  ret = ioctl(iav_fd, IAV_IOC_VIN_GET_FPS, &active_fps);
  if (0 > ret) {
    perror("IAV_IOC_VIN_GET_FPS");
    LOG_WARN("IAV_IOC_VIN_GET_FPS fail, errno %d\n", errno);
    return ret;
  }
  fps_q9 = active_fps.fps;
  __parse_fps(fps_q9, vininfo);
  vininfo->width = vin_info.info.width;
  vininfo->height = vin_info.info.height;
  vininfo->format = vin_info.info.format;
  vininfo->type = vin_info.info.type;
  vininfo->bits = vin_info.info.bits;
  vininfo->ratio = vin_info.info.ratio;
  vininfo->system = vin_info.info.system;
  vininfo->flip = vin_info.info.flip;
  vininfo->rotate = vin_info.info.rotate;
  return 0;
}

static int __s2l_get_stream_framefactor(int iav_fd, int index, SAmbaDSPStreamFramefactor *framefactor)
{
  struct iav_stream_cfg streamcfg;
  int ret = 0;
  if (DMaxStreamNumber <= index) {
    LOG_FATAL("index(%d) not as expected\n", index);
    return (-10);
  }
  memset(&streamcfg, 0, sizeof(streamcfg));
  streamcfg.id = index;
  streamcfg.cid = IAV_STMCFG_FPS;
  ret = ioctl(iav_fd, IAV_IOC_GET_STREAM_CONFIG, &streamcfg);
  if (0 > ret) {
    perror("IAV_IOC_GET_STREAM_CONFIG");
    LOG_ERROR("IAV_IOC_GET_STREAM_CONFIG fail, errno %d\n", errno);
    return ret;
  }
  framefactor->framefactor_num = streamcfg.arg.fps.fps_multi;
  framefactor->framefactor_den = streamcfg.arg.fps.fps_div;
  return 0;
}

static int __s2l_map_bsb(int iav_fd, SAmbaDSPMapBSB *map_bsb)
{
  int ret = 0;
  unsigned int map_size = 0;
  struct iav_querybuf querybuf;
  querybuf.buf = IAV_BUFFER_BSB;
  ret = ioctl(iav_fd, IAV_IOC_QUERY_BUF, &querybuf);
  if (0 > ret) {
    perror("IAV_IOC_QUERY_BUF");
    LOG_ERROR("IAV_IOC_QUERY_BUF fail\n");
    return ret;
  }
  map_bsb->size = querybuf.length;
  if (map_bsb->b_two_times) {
    map_size = querybuf.length * 2;
  } else {
    map_size = querybuf.length;
  }
  if (map_bsb->b_enable_read && map_bsb->b_enable_write) {
    map_bsb->base = mmap(NULL, map_size, PROT_WRITE | PROT_READ, MAP_SHARED, iav_fd, querybuf.offset);
  } else if (map_bsb->b_enable_read && !map_bsb->b_enable_write) {
    map_bsb->base = mmap(NULL, map_size, PROT_READ, MAP_SHARED, iav_fd, querybuf.offset);
  } else if (!map_bsb->b_enable_read && map_bsb->b_enable_write) {
    map_bsb->base = mmap(NULL, map_size, PROT_WRITE, MAP_SHARED, iav_fd, querybuf.offset);
  } else {
    LOG_ERROR("not read or write\n");
    return (-1);
  }
  if (map_bsb->base == MAP_FAILED) {
    perror("mmap");
    LOG_ERROR("mmap fail, errno %d\n", errno);
    return -1;
  }
  LOG_PRINTF("bsb_mem = %p, size = 0x%x\n", map_bsb->base, map_bsb->size);
  return 0;
}

static int __s2l_map_dsp(int iav_fd, SAmbaDSPMapDSP *map_dsp)
{
  TInt ret = 0;
  struct iav_querybuf querybuf;
  querybuf.buf = IAV_BUFFER_DSP;
  ret = ioctl(iav_fd, IAV_IOC_QUERY_BUF, &querybuf);
  if (DUnlikely(0 > ret)) {
    perror("IAV_IOC_QUERY_BUF");
    LOG_ERROR("IAV_IOC_QUERY_BUF fail, errno %d\n", errno);
    return ret;
  }
  map_dsp->size = querybuf.length;
  map_dsp->base = mmap(NULL, map_dsp->size, PROT_READ | PROT_WRITE, MAP_SHARED, iav_fd, querybuf.offset);
  if (DUnlikely(map_dsp->base == MAP_FAILED)) {
    perror("mmap");
    LOG_ERROR("mmap fail, errno %d\n", errno);
    return -1;
  }
  return 0;
}

static int __s2l_efm_get_bufferpool_info(int iav_fd, SAmbaEFMPoolInfo *buffer_pool_info)
{
  TInt ret = 0;
  struct iav_efm_get_pool_info efm_pool_info;
  memset(&efm_pool_info, 0, sizeof(efm_pool_info));
  ret = ioctl(iav_fd, IAV_IOC_EFM_GET_POOL_INFO, &efm_pool_info);
  if (0 > ret) {
    perror("IAV_IOC_EFM_GET_POOL_INFO");
    LOG_ERROR("IAV_IOC_EFM_GET_POOL_INFO fail, errno %d\n", errno);
    return ret;
  }
  if (buffer_pool_info) {
    buffer_pool_info->yuv_buf_num = efm_pool_info.yuv_buf_num;
    buffer_pool_info->yuv_pitch = efm_pool_info.yuv_pitch;
    buffer_pool_info->me1_buf_num = efm_pool_info.me1_buf_num;
    buffer_pool_info->me1_pitch = efm_pool_info.me1_pitch;
    buffer_pool_info->yuv_buf_width = efm_pool_info.yuv_size.width;
    buffer_pool_info->yuv_buf_height = efm_pool_info.yuv_size.height;
    buffer_pool_info->me1_buf_width = efm_pool_info.me1_size.width;
    buffer_pool_info->me1_buf_height = efm_pool_info.me1_size.height;
  } else {
    LOG_FATAL("NULL param\n");
    return (-2);
  }
  return 0;
}

static int __s2l_efm_request_frame(int iav_fd, SAmbaEFMFrame *frame)
{
  TInt ret = 0;
  TInt retry_cnt = 2;
  struct iav_efm_request_frame request;
  memset(&request, 0, sizeof(request));
  do {
    ret = ioctl(iav_fd, IAV_IOC_EFM_REQUEST_FRAME, &request);
    if (!ret) {
      break;
    }
    perror("IAV_IOC_EFM_REQUEST_FRAME");
    LOG_WARN("IAV_IOC_EFM_REQUEST_FRAME fail, errno %d, retry\n", errno);
    if (retry_cnt < 1) {
      return ret;
    }
    usleep(10000);
    retry_cnt --;
  } while (1);
  if (frame) {
    frame->frame_idx = request.frame_idx;
    frame->yuv_luma_offset = request.yuv_luma_offset;
    frame->yuv_chroma_offset = request.yuv_chroma_offset;
    frame->me1_offset = request.me1_offset;
  } else {
    LOG_FATAL("NULL param\n");
    return (-2);
  }
  return 0;
}

static int __s2l_efm_finish_frame(int iav_fd, SAmbaEFMFinishFrame *finish_frame)
{
  if (finish_frame) {
    TInt ret = 0;
    TInt retry_cnt = 2;
    if (!finish_frame->b_not_wait_next_interrupt) {
      ret = ioctl(iav_fd, IAV_IOC_WAIT_NEXT_FRAME, 0);
      if (0 > ret) {
        perror("IAV_IOC_WAIT_NEXT_FRAME");
        LOG_ERROR("IAV_IOC_WAIT_NEXT_FRAME fail, errno %d\n", errno);
        return ret;
      }
    }
    struct iav_efm_handshake_frame handshake;
    memset(&handshake, 0, sizeof(handshake));
    handshake.frame_idx = finish_frame->frame_idx;
    handshake.frame_pts = finish_frame->frame_pts;
    do {
      ret = ioctl(iav_fd, IAV_IOC_EFM_HANDSHAKE_FRAME, &handshake);
      if (!ret) {
        break;
      }
      perror("IAV_IOC_EFM_HANDSHAKE_FRAME");
      LOG_WARN("IAV_IOC_EFM_HANDSHAKE_FRAME fail, retry\n");
      usleep(10000);
      if (retry_cnt < 1) {
        return ret;
      }
      retry_cnt --;
    } while (1);
  } else {
    LOG_FATAL("NULL param\n");
    return (-2);
  }
  return 0;
}

static int __s2l_read_bitstream(int iav_fd, SAmbaDSPReadBitstream *bitstream)
{
  struct iav_querydesc query_desc;
  struct iav_framedesc *frame_desc;
  int ret = 0;
  memset(&query_desc, 0, sizeof(query_desc));
  frame_desc = &query_desc.arg.frame;
  query_desc.qid = IAV_DESC_FRAME;
  //frame_desc->id = bitstream->stream_id;
  frame_desc->id = 0xffffffff;
  frame_desc->time_ms = bitstream->timeout_ms;
  ret = ioctl(iav_fd, IAV_IOC_QUERY_DESC, &query_desc);
  if (ret) {
    if (EAGAIN == errno) {
      return 1;
    }
    perror("IAV_IOC_QUERY_DESC");
    LOG_ERROR("IAV_IOC_QUERY_DESC error, errno %d\n", errno);
    return (-1);
  }
  bitstream->stream_id = frame_desc->id;
  bitstream->offset = frame_desc->data_addr_offset;
  bitstream->size = frame_desc->size;
  bitstream->pts = frame_desc->arm_pts;
  bitstream->video_width = frame_desc->reso.width;
  bitstream->video_height = frame_desc->reso.height;
  //bitstream->slice_id = frame_desc->slice_id;
  //bitstream->slice_num = frame_desc->slice_num;
  if (frame_desc->stream_end) {
    DASSERT(!bitstream->size);
    bitstream->size = 0;
    bitstream->offset = 0;
  }
  if (IAV_STREAM_TYPE_H264 == frame_desc->stream_type) {
    bitstream->stream_type = StreamFormat_H264;
  } /*else if (IAV_STREAM_TYPE_H265 == frame_desc->stream_type) {
        bitstream->stream_type = StreamFormat_H265;
    }*/ else if (IAV_STREAM_TYPE_MJPEG == frame_desc->stream_type) {
    bitstream->stream_type = StreamFormat_JPEG;
  } else {
    bitstream->stream_type = StreamFormat_Invalid;
    LOG_FATAL("bad stream type %d\n", bitstream->stream_type);
    return (-2);
  }
  return 0;
}

static int __s2l_is_ready_for_read_bitstream(int iav_fd)
{
  return 1;
}

static void __setup_s2l_al_context(SFAmbaDSPDecAL *al)
{
  al->f_enter_mode = __s2l_enter_decode_mode;
  al->f_leave_mode = __s2l_leave_decode_mode;
  al->f_create_decoder = __s2l_create_decoder;
  al->f_destroy_decoder = __s2l_destroy_decoder;
  al->f_trickplay = __s2l_decode_trick_play;
  al->f_start = __s2l_decode_start;
  al->f_stop = __s2l_decode_stop;
  al->f_speed = __s2l_decode_speed;
  al->f_request_bsb = __s2l_decode_request_bits_fifo;
  al->f_decode = __s2l_decode;
  al->f_query_print_decode_bsb_status = __s2l_decode_query_bsb_status_and_print;
  al->f_query_print_decode_status = __s2l_decode_query_status_and_print;
  al->f_query_decode_bsb_status = __s2l_decode_query_bsb_status;
  al->f_query_decode_status = __s2l_decode_query_status;
  al->f_get_vout_info = __s2l_get_single_vout_info;
  al->f_get_vin_info = __s2l_get_vin_info;
  al->f_get_stream_framefactor = __s2l_get_stream_framefactor;
  al->f_map_bsb = __s2l_map_bsb;
  al->f_map_dsp = __s2l_map_dsp;
  al->f_map_intrapb = NULL;
  al->f_efm_get_buffer_pool_info = __s2l_efm_get_bufferpool_info;
  al->f_efm_request_frame = __s2l_efm_request_frame;
  al->f_efm_finish_frame = __s2l_efm_finish_frame;
  al->f_read_bitstream = __s2l_read_bitstream;
  al->f_is_ready_for_read_bitstream = __s2l_is_ready_for_read_bitstream;
  al->f_intraplay_reset_buffers = NULL;
  al->f_intraplay_request_buffer = NULL;
  al->f_intraplay_decode = NULL;
  al->f_intraplay_yuv2yuv = NULL;
  al->f_intraplay_display = NULL;
}

#elif defined (BUILD_DSP_AMBA_S2) || defined (BUILD_DSP_AMBA_S2E)
#include "errno.h"
#include "basetypes.h"
#include "ambas_common.h"
#include "ambas_vout.h"
#include "ambas_vin.h"
#include "iav_drv.h"

static int __s2e_get_single_vout_info(int iav_fd, int chan, int type, SAmbaDSPVoutInfo *voutinfo)
{
  int num;
  int sink_type = 0;
  int i;
  struct amba_vout_sink_info  sink_info;
  if (EAMDSP_VOUT_TYPE_DIGITAL == type) {
    sink_type = AMBA_VOUT_SINK_TYPE_DIGITAL;
  } else if (EAMDSP_VOUT_TYPE_HDMI == type) {
    sink_type = AMBA_VOUT_SINK_TYPE_HDMI;
  } else if (EAMDSP_VOUT_TYPE_CVBS == type) {
    sink_type = AMBA_VOUT_SINK_TYPE_CVBS;
  } else {
    LOG_ERROR("not valid type %d\n", type);
    return (-1);
  }
  num = 0;
  if (ioctl(iav_fd, IAV_IOC_VOUT_GET_SINK_NUM, &num) < 0) {
    perror("IAV_IOC_VOUT_GET_SINK_NUM");
    LOG_ERROR("IAV_IOC_VOUT_GET_SINK_NUM fail, errno %d\n", errno);
    return (-2);
  }
  if (num < 1) {
    LOG_PRINTF("Please load vout driver!\n");
    return (-3);
  }
  for (i = 0; i < num; i++) {
    sink_info.id = i;
    if (ioctl(iav_fd, IAV_IOC_VOUT_GET_SINK_INFO, &sink_info) < 0) {
      perror("IAV_IOC_VOUT_GET_SINK_INFO");
      LOG_ERROR("IAV_IOC_VOUT_GET_SINK_NUM fail, errno %d\n", errno);
      return (-4);
    }
    if ((sink_info.state == AMBA_VOUT_SINK_STATE_RUNNING) && (sink_info.sink_type == sink_type) && (sink_info.source_id == chan)) {
      if (voutinfo) {
        voutinfo->sink_id = sink_info.id;
        voutinfo->source_id = sink_info.source_id;
        voutinfo->sink_type = sink_info.sink_type;
        voutinfo->width = sink_info.sink_mode.video_size.vout_width;
        voutinfo->height = sink_info.sink_mode.video_size.vout_height;
        voutinfo->offset_x = sink_info.sink_mode.video_offset.offset_x;
        voutinfo->offset_y = sink_info.sink_mode.video_offset.offset_y;
        voutinfo->rotate = sink_info.sink_mode.video_rotate;
        voutinfo->flip = sink_info.sink_mode.video_flip;
        voutinfo->mode =  sink_info.sink_mode.mode;
        return 0;
      }
    }
  }
  LOG_NOTICE("no vout with type %d, id %d\n", type, chan);
  return (-5);
}

#if 0
static void __parse_fps(TU32 fps_q9, SAmbaDSPVinInfo *vininfo)
{
  switch (fps_q9) {
    case AMBA_VIDEO_FPS_AUTO:
      vininfo->fps = 30;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3003;
      LOG_CONFIGURATION("auto fps, set 29.97 as default\n");
      break;
    case AMBA_VIDEO_FPS_29_97:
      vininfo->fps = 30;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3003;
      break;
    case AMBA_VIDEO_FPS_30:
      vininfo->fps = 30;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3000;
      break;
    case AMBA_VIDEO_FPS_59_94:
      vininfo->fps = 60;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 1501;
      break;
    case AMBA_VIDEO_FPS_60:
      vininfo->fps = 60;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 1500;
      break;
    case AMBA_VIDEO_FPS_120:
      vininfo->fps = 120;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 750;
      break;
    case AMBA_VIDEO_FPS_23_976:
      vininfo->fps = 24;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3754;
      break;
    case AMBA_VIDEO_FPS_24:
      vininfo->fps = 24;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3750;
      break;
    case AMBA_VIDEO_FPS_25:
      vininfo->fps = 25;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3600;
      break;
    case AMBA_VIDEO_FPS_20:
      vininfo->fps = 20;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 4500;
      break;
    case AMBA_VIDEO_FPS_15:
      vininfo->fps = 15;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 6000;
      break;
    case AMBA_VIDEO_FPS_12:
      vininfo->fps = 12;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 7500;
      break;
    case AMBA_VIDEO_FPS_10:
      vininfo->fps = 10;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 9000;
      break;
    case AMBA_VIDEO_FPS_6:
      vininfo->fps = 6;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 15000;
      break;
    case AMBA_VIDEO_FPS_5:
      vininfo->fps = 5;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 18000;
      break;
    case AMBA_VIDEO_FPS_4:
      vininfo->fps = 4;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 22500;
      break;
    case AMBA_VIDEO_FPS_3:
      vininfo->fps = 3;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 30000;
      break;
    case AMBA_VIDEO_FPS_2:
      vininfo->fps = 2;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 45000;
      break;
    case AMBA_VIDEO_FPS_1:
      vininfo->fps = 1;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 90000;
      break;
    default:
      vininfo->fps = (TU64)((TU64) 512000000 + (fps_q9 >> 1)) / (TU64)(fps_q9);
      vininfo->fr_num = 90000;
      vininfo->fr_den = (float) 90000 * (float)(fps_q9) / ((float)((TU64) 512000000 + (TU64)(fps_q9 >> 1)));
      break;
  }
  return;
}

#else

static void __parse_fps(u32 fps_q9, SAmbaDSPVinInfo *vininfo)
{
  switch (fps_q9) {
    case AMBA_VIDEO_FPS_AUTO:
      vininfo->fps = 30;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3003;
      break;
    case AMBA_VIDEO_FPS_29_97:
      vininfo->fps = 30;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3003;
      break;
    case AMBA_VIDEO_FPS_59_94:
      vininfo->fps = 60;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 1501;
      break;
    default:
      vininfo->fps = (TU64)((TU64) 512000000 + (fps_q9 >> 1)) / (TU64)(fps_q9);
      vininfo->fr_num = 90000;
      vininfo->fr_den = (TU64) 90000 * (TU64)(fps_q9) / (512000000);
      break;
  }
}

#endif

static int __s2e_get_vin_info(int iav_fd, SAmbaDSPVinInfo *vininfo)
{
  struct amba_video_info vin_info;
  TU32 fps_q9 = 1;
  int ret = 0;
  ret = ioctl(iav_fd, IAV_IOC_VIN_SRC_GET_VIDEO_INFO, &vin_info);
  if (0 > ret) {
    perror("IAV_IOC_VIN_SRC_GET_VIDEO_INFO");
    LOG_ERROR("IAV_IOC_VIN_SRC_GET_VIDEO_INFO fail, errno %d\n", errno);
    return ret;
  }
  ret = ioctl(iav_fd, IAV_IOC_VIN_SRC_GET_FRAME_RATE, &fps_q9);
  if (0 > ret) {
    perror("IAV_IOC_VIN_SRC_GET_FRAME_RATE");
    LOG_WARN("IAV_IOC_VIN_SRC_GET_FRAME_RATE fail, errno %d\n", errno);
    return ret;
  }
  __parse_fps(fps_q9, vininfo);
  vininfo->width = vin_info.width;
  vininfo->height = vin_info.height;
  vininfo->format = vin_info.format;
  vininfo->type = vin_info.type;
  vininfo->bits = vin_info.bits;
  vininfo->ratio = vin_info.ratio;
  vininfo->system = vin_info.system;
  vininfo->flip = vin_info.flip;
  vininfo->rotate = vin_info.rotate;
  vininfo->pattern = vin_info.pattern;
  return 0;
}

static int __s2e_get_stream_framefactor(int iav_fd, int index, SAmbaDSPStreamFramefactor *framefactor)
{
  iav_change_framerate_factor_ex_t frame_factor;
  int ret = 0;
  if (DMaxStreamNumber <= index) {
    LOG_FATAL("index(%d) not as expected\n", index);
    return (-10);
  }
  memset(&frame_factor, 0, sizeof(frame_factor));
  frame_factor.id = 1 << index;
  ret = ioctl(iav_fd, IAV_IOC_GET_FRAMERATE_FACTOR_EX, &frame_factor);
  if (0 > ret) {
    perror("IAV_IOC_GET_FRAMERATE_FACTOR_EX");
    LOG_ERROR("IAV_IOC_GET_FRAMERATE_FACTOR_EX fail, errno %d\n", errno);
    return ret;
  }
  framefactor->framefactor_num = frame_factor.ratio_numerator;
  framefactor->framefactor_den = frame_factor.ratio_denominator;
  return 0;
}

static int __s2e_map_bsb(int iav_fd, SAmbaDSPMapBSB *map_bsb)
{
  int ret = 0;
  iav_mmap_info_t info;
  ret = ioctl(iav_fd, IAV_IOC_MAP_BSB2, &info);
  if (0 > ret) {
    perror("IAV_IOC_MAP_BSB2");
    LOG_ERROR("IAV_IOC_MAP_BSB2 fail, errno %d\n", errno);
    return (-1);
  }
  map_bsb->base = info.addr;
  map_bsb->size = info.length;
  LOG_PRINTF("bsb_mem = %p, size = 0x%x\n", map_bsb->base, map_bsb->size);
  return 0;
}

static int __s2e_read_bitstream(int iav_fd, SAmbaDSPReadBitstream *bitstream)
{
  bits_info_ex_t  bits_info;
  int ret = 0;
  memset(&bits_info, 0, sizeof(bits_info_ex_t));
  bits_info.time_ms = bitstream->timeout_ms;
  ret = ioctl(iav_fd, IAV_IOC_READ_BITSTREAM_EX, &bits_info);
  if (EAGAIN == errno) {
    return 1;
  } else if (EIO == errno) {
    LOG_NOTICE("IAV_IOC_READ_BITSTREAM_EX ret EIO\n");
    return 2;
  }
  if (0 > ret) {
    perror("IAV_IOC_READ_BITSTREAM_EX");
    LOG_ERROR("IAV_IOC_READ_BITSTREAM_EX fail, errno %d\n", errno);
    return ret;
  }
  bitstream->stream_id = bits_info.stream_id;
  bitstream->offset = bits_info.data_addr_offset;
  bitstream->size = bits_info.pic_size;
  bitstream->pts = bits_info.PTS;
  bitstream->video_width = bits_info.pic_width;
  bitstream->video_height = bits_info.pic_height;
  if (bits_info.stream_end) {
    bitstream->size = 0;
    bitstream->offset = 0;
    return 0;
  }
  if (P_FRAME == bits_info.pic_type) {
    bitstream->stream_type = StreamFormat_H264;
    bitstream->hint_frame_type = EPredefinedPictureType_P;
    bitstream->hint_is_keyframe = 0;
  } else if (IDR_FRAME == bits_info.pic_type) {
    bitstream->stream_type = StreamFormat_H264;
    bitstream->hint_frame_type = EPredefinedPictureType_IDR;
    bitstream->hint_is_keyframe = 1;
  } else if (I_FRAME == bits_info.pic_type) {
    bitstream->stream_type = StreamFormat_H264;
    bitstream->hint_frame_type = EPredefinedPictureType_I;
    bitstream->hint_is_keyframe = 0;
  } else if (B_FRAME == bits_info.pic_type) {
    bitstream->stream_type = StreamFormat_H264;
    bitstream->hint_frame_type = EPredefinedPictureType_B;
    bitstream->hint_is_keyframe = 0;
  } else if (JPEG_STREAM == bits_info.pic_type) {
    bitstream->stream_type = StreamFormat_JPEG;
    bitstream->hint_frame_type = EPredefinedPictureType_IDR;
    bitstream->hint_is_keyframe = 1;
  } else if ((IAV_PIC_TYPE_LAST > bits_info.pic_type) && (IAV_PIC_TYPE_FIRST <= bits_info.pic_type)) {
    bitstream->stream_type = StreamFormat_Invalid;
    LOG_NOTICE("not supported pic_type %d\n", bits_info.pic_type);
  } else {
    bitstream->stream_type = StreamFormat_Invalid;
    LOG_FATAL("bad pic type %d\n", bits_info.pic_type);
    return (-2);
  }
  return 0;
}

static int __s2e_is_ready_for_read_bitstream(int iav_fd)
{
  unsigned int i = 0;
  iav_encode_stream_info_ex_t info;
  for (i = 0; i < IAV_STREAM_MAX_NUM_IMPL; ++ i) {
    info.id = (1 << i);
    if (ioctl(iav_fd, IAV_IOC_GET_ENCODE_STREAM_INFO_EX, &info) < 0) {
      perror("IAV_IOC_GET_ENCODE_STREAM_INFO_EX");
      LOG_ERROR("IAV_IOC_GET_ENCODE_STREAM_INFO_EX error\n");
      return 0;
    }
    if (info.state == IAV_STREAM_STATE_ENCODING) {
      return 1;
    }
  }
  return 0;
}

static void __setup_s2_s2e_al_context(SFAmbaDSPDecAL *al)
{
  al->f_enter_mode = NULL;
  al->f_leave_mode = NULL;
  al->f_create_decoder = NULL;
  al->f_destroy_decoder = NULL;
  al->f_trickplay = NULL;
  al->f_start = NULL;
  al->f_stop = NULL;
  al->f_speed = NULL;
  al->f_request_bsb = NULL;
  al->f_decode = NULL;
  al->f_query_print_decode_bsb_status = NULL;
  al->f_query_print_decode_status = NULL;
  al->f_query_decode_bsb_status = NULL;
  al->f_query_decode_status = NULL;
  al->f_get_vout_info = __s2e_get_single_vout_info;
  al->f_get_vin_info = __s2e_get_vin_info;
  al->f_get_stream_framefactor = __s2e_get_stream_framefactor;
  al->f_map_bsb = __s2e_map_bsb;
  al->f_map_dsp = NULL;
  al->f_map_intrapb = NULL;
  al->f_efm_get_buffer_pool_info = NULL;
  al->f_efm_request_frame = NULL;
  al->f_efm_finish_frame = NULL;
  al->f_read_bitstream = __s2e_read_bitstream;
  al->f_is_ready_for_read_bitstream = __s2e_is_ready_for_read_bitstream;
  al->f_intraplay_reset_buffers = NULL;
  al->f_intraplay_request_buffer = NULL;
  al->f_intraplay_decode = NULL;
  al->f_intraplay_yuv2yuv = NULL;
  al->f_intraplay_display = NULL;
}

#elif defined (BUILD_DSP_AMBA_S3L)

#include "errno.h"
#include "basetypes.h"
#include "iav_ioctl.h"

static TInt __s3l_enter_decode_mode(TInt iav_fd, SAmbaDSPDecodeModeConfig *mode_config)
{
  struct iav_decode_mode_config decode_mode;
  TInt i = 0;
  memset(&decode_mode, 0x0, sizeof(decode_mode));
  if (mode_config->num_decoder > DAMBADSP_MAX_DECODER_NUMBER) {
    LOG_FATAL("BAD num_decoder %d\n", mode_config->num_decoder);
    return (-100);
  }
  decode_mode.num_decoder = mode_config->num_decoder;
  for (i = 0; i < decode_mode.num_decoder; i ++) {
    decode_mode.decoder_configs[i].max_frm_num = mode_config->decoder_configs[i].max_frm_num;
    decode_mode.decoder_configs[i].max_frm_width = mode_config->decoder_configs[i].max_frm_width;
    decode_mode.decoder_configs[i].max_frm_height = mode_config->decoder_configs[i].max_frm_height;
    decode_mode.decoder_configs[i].b_support_ff = mode_config->decoder_configs[i].b_support_ff;
    decode_mode.decoder_configs[i].b_support_fb = mode_config->decoder_configs[i].b_support_fb;
    decode_mode.decoder_configs[i].b_support_bw = mode_config->decoder_configs[i].b_support_bw;
  }
  i = ioctl(iav_fd, IAV_IOC_ENTER_DECODE_MODE, &decode_mode);
  if (0 > i) {
    perror("IAV_IOC_ENTER_UDEC_MODE");
    LOG_FATAL("enter decode mode fail, errno %d\n", errno);
    return i;
  }
  return 0;
}

static TInt __s3l_leave_decode_mode(TInt iav_fd)
{
  TInt ret = ioctl(iav_fd, IAV_IOC_LEAVE_DECODE_MODE);
  if (0 > ret) {
    perror("IAV_IOC_LEAVE_DECODE_MODE");
    LOG_FATAL("leave decode mode fail, errno %d\n", errno);
  }
  return ret;
}

static TInt __s3l_create_decoder(TInt iav_fd, SAmbaDSPDecoderInfo *p_decoder_info)
{
  TInt i = 0;
  struct iav_decoder_info decoder_info;
  memset(&decoder_info, 0x0, sizeof(decoder_info));
  decoder_info.decoder_id = p_decoder_info->decoder_id;
  if (EAMDSP_VIDEO_CODEC_TYPE_H264 == p_decoder_info->decoder_type) {
    decoder_info.decoder_type = IAV_DECODER_TYPE_H264;
  } else if (EAMDSP_VIDEO_CODEC_TYPE_H265 == p_decoder_info->decoder_type) {
    decoder_info.decoder_type = IAV_DECODER_TYPE_H265;
  } else {
    LOG_FATAL("bad video codec type %d\n", p_decoder_info->decoder_type);
    return (-101);
  }
  decoder_info.num_vout = p_decoder_info->num_vout;
  decoder_info.setup_done = p_decoder_info->setup_done;
  decoder_info.width = p_decoder_info->width;
  decoder_info.height = p_decoder_info->height;
  if (decoder_info.num_vout > DAMBADSP_MAX_DECODE_VOUT_NUMBER) {
    LOG_FATAL("BAD num_vout %d\n", p_decoder_info->num_vout);
    return (-100);
  }
  for (i = 0; i < decoder_info.num_vout; i ++) {
    decoder_info.vout_configs[i].vout_id = p_decoder_info->vout_configs[i].vout_id;
    decoder_info.vout_configs[i].enable = p_decoder_info->vout_configs[i].enable;
    decoder_info.vout_configs[i].flip = p_decoder_info->vout_configs[i].flip;
    decoder_info.vout_configs[i].rotate = p_decoder_info->vout_configs[i].rotate;
    decoder_info.vout_configs[i].target_win_offset_x = p_decoder_info->vout_configs[i].target_win_offset_x;
    decoder_info.vout_configs[i].target_win_offset_y = p_decoder_info->vout_configs[i].target_win_offset_y;
    decoder_info.vout_configs[i].target_win_width = p_decoder_info->vout_configs[i].target_win_width;
    decoder_info.vout_configs[i].target_win_height = p_decoder_info->vout_configs[i].target_win_height;
    decoder_info.vout_configs[i].zoom_factor_x = p_decoder_info->vout_configs[i].zoom_factor_x;
    decoder_info.vout_configs[i].zoom_factor_y = p_decoder_info->vout_configs[i].zoom_factor_y;
    decoder_info.vout_configs[i].vout_mode = p_decoder_info->vout_configs[i].vout_mode;
    LOG_PRINTF("vout(%d), w %d, h %d, zoom x %08x, y %08x\n", i, decoder_info.vout_configs[i].target_win_width, decoder_info.vout_configs[i].target_win_height, decoder_info.vout_configs[i].zoom_factor_x, decoder_info.vout_configs[i].zoom_factor_y);
  }
  decoder_info.bsb_start_offset = p_decoder_info->bsb_start_offset;
  decoder_info.bsb_size = p_decoder_info->bsb_size;
  i = ioctl(iav_fd, IAV_IOC_CREATE_DECODER, &decoder_info);
  if (0 > i) {
    perror("IAV_IOC_CREATE_DECODER");
    LOG_FATAL("create decoder fail, errno %d\n", errno);
    return i;
  }
  p_decoder_info->bsb_start_offset = decoder_info.bsb_start_offset;
  p_decoder_info->bsb_size = decoder_info.bsb_size;
  return 0;
}

static TInt __s3l_destroy_decoder(TInt iav_fd, TU8 decoder_id)
{
  TInt ret = ioctl(iav_fd, IAV_IOC_DESTROY_DECODER, decoder_id);
  if (0 > ret) {
    perror("IAV_IOC_DESTROY_DECODER");
    LOG_FATAL("destroy decoder fail, errno %d\n", errno);
  }
  return ret;
}

static int __s3l_decode_trick_play(int iav_fd, TU8 decoder_id, TU8 trick_play)
{
  int ret;
  struct iav_decode_trick_play trickplay;
  trickplay.decoder_id = decoder_id;
  trickplay.trick_play = trick_play;
  ret = ioctl(iav_fd, IAV_IOC_DECODE_TRICK_PLAY, &trickplay);
  if (0 > ret) {
    perror("IAV_IOC_DECODE_TRICK_PLAY");
    LOG_ERROR("trickplay error, errno %d\n", errno);
    return ret;
  }
  return 0;
}

static int __s3l_decode_start(int iav_fd, TU8 decoder_id)
{
  int ret = ioctl(iav_fd, IAV_IOC_DECODE_START, decoder_id);
  if (ret < 0) {
    perror("IAV_IOC_DECODE_START");
    LOG_ERROR("decode start error, errno %d\n", errno);
    return ret;
  }
  return 0;
}

static int __s3l_decode_stop(int iav_fd, TU8 decoder_id, TU8 stop_flag)
{
  int ret;
  struct iav_decode_stop stop;
  stop.decoder_id = decoder_id;
  stop.stop_flag = stop_flag;
  ret = ioctl(iav_fd, IAV_IOC_DECODE_STOP, &stop);
  if (0 > ret) {
    perror("IAV_IOC_UDEC_STOP");
    LOG_ERROR("decode stop error, errno %d\n", errno);
    return ret;
  }
  return 0;
}

static int __s3l_decode_speed(int iav_fd, TU8 decoder_id, TU16 speed, TU8 scan_mode, TU8 direction)
{
  int ret;
  struct iav_decode_speed spd;
  spd.decoder_id = decoder_id;
  spd.direction = direction;
  spd.speed = speed;
  spd.scan_mode = scan_mode;
  LOG_PRINTF("speed, direction %d, speed %x, scanmode %d\n", spd.direction, spd.speed, spd.scan_mode);
  ret = ioctl(iav_fd, IAV_IOC_DECODE_SPEED, &spd);
  if (0 > ret) {
    perror("IAV_IOC_DECODE_SPEED");
    LOG_ERROR("decode speed error, errno %d\n", errno);
    return ret;
  }
  return 0;
}

static int __s3l_decode_request_bits_fifo(int iav_fd, int decoder_id, TU32 size, void *cur_pos_offset)
{
  struct iav_decode_bsb wait;
  int ret;
  wait.decoder_id = decoder_id;
  wait.room = size;
  wait.start_offset = (TU32) cur_pos_offset;
  ret = ioctl(iav_fd, IAV_IOC_WAIT_DECODE_BSB, &wait);
  if (0 > ret) {
    LOG_ERROR("IAV_IOC_WAIT_DECODE_BSB fail, errno %d.\n", errno);
    perror("IAV_IOC_WAIT_DECODE_BSB");
    return ret;
  }
  return 0;
}

static int __s3l_decode(int iav_fd, SAmbaDSPDecode *dec)
{
  TInt ret = 0;
  struct iav_decode_video decode_video;
  memset(&decode_video, 0, sizeof(decode_video));
  decode_video.decoder_id = dec->decoder_id;
  decode_video.num_frames = dec->num_frames;
  decode_video.start_ptr_offset = dec->start_ptr_offset;
  decode_video.end_ptr_offset = dec->end_ptr_offset;
  decode_video.first_frame_display = dec->first_frame_display;
  ret = ioctl(iav_fd, IAV_IOC_DECODE_VIDEO, &decode_video);
  if (0 > ret) {
    perror("IAV_IOC_DECODE_VIDEO");
    LOG_ERROR("IAV_IOC_DECODE_VIDEO fail, errno %d.\n", errno);
    return ret;
  }
  return 0;
}

static int __s3l_decode_query_bsb_status_and_print(int iav_fd, TU8 decoder_id)
{
  int ret;
  struct iav_decode_bsb bsb;
  memset(&bsb, 0x0, sizeof(bsb));
  bsb.decoder_id = decoder_id;
  ret = ioctl(iav_fd, IAV_IOC_QUERY_DECODE_BSB, &bsb);
  if (0 > ret) {
    LOG_ERROR("IAV_IOC_QUERY_DECODE_BSB fail, errno %d.\n", errno);
    perror("IAV_IOC_QUERY_DECODE_BSB");
    return ret;
  }
  LOG_PRINTF("[bsb]: current write offset (arm) 0x%08x, current read offset (dsp) 0x%08x, safe room (minus 256 bytes) %d, free room %d\n", bsb.start_offset, bsb.dsp_read_offset, bsb.room, bsb.free_room);
  return 0;
}

static int __s3l_decode_query_status_and_print(int iav_fd, TU8 decoder_id)
{
  int ret;
  struct iav_decode_status status;
  memset(&status, 0x0, sizeof(status));
  status.decoder_id = decoder_id;
  ret = ioctl(iav_fd, IAV_IOC_QUERY_DECODE_STATUS, &status);
  if (0 > ret) {
    perror("IAV_IOC_QUERY_DECODE_STATUS");
    LOG_ERROR("IAV_IOC_QUERY_DECODE_STATUS fail, errno %d.\n", errno);
    return ret;
  }
  LOG_PRINTF("[decode status]: decode_state %d, decoded_pic_number %d, error_status %d, total_error_count %d, irq_count %d\n", status.decode_state, status.decoded_pic_number, status.error_status, status.total_error_count, status.irq_count);
  LOG_PRINTF("[decode status, bsb]: current write offset (arm) 0x%08x, current read offset (dsp) 0x%08x, safe room (minus 256 bytes) %d, free room %d\n", status.write_offset, status.dsp_read_offset, status.room, status.free_room);
  LOG_PRINTF("[decode status, last pts]: %d, is_started %d, is_send_stop_cmd %d\n", status.last_pts, status.is_started, status.is_send_stop_cmd);
  LOG_PRINTF("[decode status, yuv addr]: yuv422_y 0x%08x, yuv422_uv 0x%08x\n", status.yuv422_y_addr, status.yuv422_uv_addr);
  return 0;
}

static int __s3l_decode_query_bsb_status(int iav_fd, SAmbaDSPBSBStatus *status)
{
  int ret;
  struct iav_decode_bsb bsb;
  memset(&bsb, 0x0, sizeof(bsb));
  bsb.decoder_id = status->decoder_id;
  ret = ioctl(iav_fd, IAV_IOC_QUERY_DECODE_BSB, &bsb);
  if (0 > ret) {
    LOG_ERROR("IAV_IOC_QUERY_DECODE_BSB fail, errno %d.\n", errno);
    perror("IAV_IOC_QUERY_DECODE_BSB");
    return ret;
  }
  status->start_offset = bsb.start_offset;
  status->room = bsb.room;
  status->dsp_read_offset = bsb.dsp_read_offset;
  status->free_room = bsb.free_room;
  return 0;
}

static int __s3l_decode_query_status(int iav_fd, SAmbaDSPDecodeStatus *status)
{
  int ret;
  struct iav_decode_status dec_status;
  memset(&dec_status, 0x0, sizeof(dec_status));
  dec_status.decoder_id = status->decoder_id;
  ret = ioctl(iav_fd, IAV_IOC_QUERY_DECODE_STATUS, &dec_status);
  if (0 > ret) {
    perror("IAV_IOC_QUERY_DECODE_STATUS");
    LOG_ERROR("IAV_IOC_QUERY_DECODE_STATUS fail, errno %d.\n", errno);
    return ret;
  }
  status->is_started = dec_status.is_started;
  status->is_send_stop_cmd = dec_status.is_send_stop_cmd;
  status->last_pts = dec_status.last_pts;
  status->decode_state = dec_status.decode_state;
  status->error_status = dec_status.error_status;
  status->total_error_count = dec_status.total_error_count;
  status->decoded_pic_number = dec_status.decoded_pic_number;
  status->write_offset = dec_status.write_offset;
  status->room = dec_status.room;
  status->dsp_read_offset = dec_status.dsp_read_offset;
  status->free_room = dec_status.free_room;
  status->irq_count = dec_status.irq_count;
  status->yuv422_y_addr = dec_status.yuv422_y_addr;
  status->yuv422_uv_addr = dec_status.yuv422_uv_addr;
  return 0;
}

static int __s3l_get_single_vout_info(int iav_fd, int chan, int type, SAmbaDSPVoutInfo *voutinfo)
{
  int num;
  int sink_type = 0;
  int i;
  struct amba_vout_sink_info  sink_info;
  if (EAMDSP_VOUT_TYPE_DIGITAL == type) {
    sink_type = AMBA_VOUT_SINK_TYPE_DIGITAL;
  } else if (EAMDSP_VOUT_TYPE_HDMI == type) {
    sink_type = AMBA_VOUT_SINK_TYPE_HDMI;
  } else if (EAMDSP_VOUT_TYPE_CVBS == type) {
    sink_type = AMBA_VOUT_SINK_TYPE_CVBS;
  } else {
    LOG_ERROR("not valid type %d\n", type);
    return (-1);
  }
  num = 0;
  if (ioctl(iav_fd, IAV_IOC_VOUT_GET_SINK_NUM, &num) < 0) {
    perror("IAV_IOC_VOUT_GET_SINK_NUM");
    LOG_ERROR("IAV_IOC_VOUT_GET_SINK_NUM fail, errno %d\n", errno);
    return (-2);
  }
  if (num < 1) {
    LOG_PRINTF("Please load vout driver!\n");
    return (-3);
  }
  for (i = 0; i < num; i++) {
    sink_info.id = i;
    if (ioctl(iav_fd, IAV_IOC_VOUT_GET_SINK_INFO, &sink_info) < 0) {
      perror("IAV_IOC_VOUT_GET_SINK_INFO");
      LOG_ERROR("IAV_IOC_VOUT_GET_SINK_NUM fail, errno %d\n", errno);
      return (-4);
    }
    if ((sink_info.state == AMBA_VOUT_SINK_STATE_RUNNING) && (sink_info.sink_type == sink_type) && (sink_info.source_id == chan)) {
      if (voutinfo) {
        voutinfo->sink_id = sink_info.id;
        voutinfo->source_id = sink_info.source_id;
        voutinfo->sink_type = sink_info.sink_type;
        voutinfo->width = sink_info.sink_mode.video_size.vout_width;
        voutinfo->height = sink_info.sink_mode.video_size.vout_height;
        voutinfo->offset_x = sink_info.sink_mode.video_offset.offset_x;
        voutinfo->offset_y = sink_info.sink_mode.video_offset.offset_y;
        voutinfo->rotate = sink_info.sink_mode.video_rotate;
        voutinfo->flip = sink_info.sink_mode.video_flip;
        voutinfo->mode =  sink_info.sink_mode.mode;
        return 0;
      }
    }
  }
  LOG_NOTICE("no vout with type %d, id %d\n", type, chan);
  return (-5);
}

#if 0
static void __parse_fps(TU32 fps_q9, SAmbaDSPVinInfo *vininfo)
{
  switch (fps_q9) {
    case AMBA_VIDEO_FPS_AUTO:
      vininfo->fps = 30;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3003;
      LOG_CONFIGURATION("auto fps, set 29.97 as default\n");
      break;
    case AMBA_VIDEO_FPS_29_97:
      vininfo->fps = 30;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3003;
      break;
    case AMBA_VIDEO_FPS_30:
      vininfo->fps = 30;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3000;
      break;
    case AMBA_VIDEO_FPS_59_94:
      vininfo->fps = 60;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 1501;
      break;
    case AMBA_VIDEO_FPS_60:
      vininfo->fps = 60;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 1500;
      break;
    case AMBA_VIDEO_FPS_120:
      vininfo->fps = 120;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 750;
      break;
    case AMBA_VIDEO_FPS_23_976:
      vininfo->fps = 24;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3754;
      break;
    case AMBA_VIDEO_FPS_24:
      vininfo->fps = 24;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3750;
      break;
    case AMBA_VIDEO_FPS_25:
      vininfo->fps = 25;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3600;
      break;
    case AMBA_VIDEO_FPS_20:
      vininfo->fps = 20;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 4500;
      break;
    case AMBA_VIDEO_FPS_15:
      vininfo->fps = 15;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 6000;
      break;
    case AMBA_VIDEO_FPS_12:
      vininfo->fps = 12;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 7500;
      break;
    case AMBA_VIDEO_FPS_10:
      vininfo->fps = 10;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 9000;
      break;
    case AMBA_VIDEO_FPS_6:
      vininfo->fps = 6;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 15000;
      break;
    case AMBA_VIDEO_FPS_5:
      vininfo->fps = 5;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 18000;
      break;
    case AMBA_VIDEO_FPS_4:
      vininfo->fps = 4;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 22500;
      break;
    case AMBA_VIDEO_FPS_3:
      vininfo->fps = 3;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 30000;
      break;
    case AMBA_VIDEO_FPS_2:
      vininfo->fps = 2;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 45000;
      break;
    case AMBA_VIDEO_FPS_1:
      vininfo->fps = 1;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 90000;
      break;
    default:
      vininfo->fps = (TU64)((TU64) 512000000 + (fps_q9 >> 1)) / (TU64)(fps_q9);
      vininfo->fr_num = 90000;
      vininfo->fr_den = (float) 90000 * (float)(fps_q9) / ((float)((TU64) 512000000 + (TU64)(fps_q9 >> 1)));
      break;
  }
  return;
}

#else

static void __parse_fps(u32 fps_q9, SAmbaDSPVinInfo *vininfo)
{
  switch (fps_q9) {
    case AMBA_VIDEO_FPS_AUTO:
      vininfo->fps = 30;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3003;
      break;
    case AMBA_VIDEO_FPS_29_97:
      vininfo->fps = 30;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 3003;
      break;
    case AMBA_VIDEO_FPS_59_94:
      vininfo->fps = 60;
      vininfo->fr_num = 90000;
      vininfo->fr_den = 1501;
      break;
    default:
      vininfo->fps = (TU64)((TU64) 512000000 + (fps_q9 >> 1)) / (TU64)(fps_q9);
      vininfo->fr_num = 90000;
      vininfo->fr_den = (TU64) 90000 * (TU64)(fps_q9) / (512000000);
      break;
  }
}

#endif

static int __s3l_get_vin_info(int iav_fd, SAmbaDSPVinInfo *vininfo)
{
  struct vindev_video_info vin_info;
  struct vindev_fps active_fps;
  TU32 fps_q9 = 1;
  int ret = 0;
  memset(&vin_info, 0x0, sizeof(vin_info));
  vin_info.vsrc_id = 0;
  vin_info.info.mode = AMBA_VIDEO_MODE_CURRENT;
  ret = ioctl(iav_fd, IAV_IOC_VIN_GET_VIDEOINFO, &vin_info);
  if (0 > ret) {
    perror("IAV_IOC_VIN_GET_VIDEOINFO");
    LOG_WARN("IAV_IOC_VIN_GET_VIDEOINFO fail, errno %d\n", errno);
    return ret;
  }
  memset(&active_fps, 0, sizeof(active_fps));
  active_fps.vsrc_id = 0;
  ret = ioctl(iav_fd, IAV_IOC_VIN_GET_FPS, &active_fps);
  if (0 > ret) {
    perror("IAV_IOC_VIN_GET_FPS");
    LOG_WARN("IAV_IOC_VIN_GET_FPS fail, errno %d\n", errno);
    return ret;
  }
  fps_q9 = active_fps.fps;
  __parse_fps(fps_q9, vininfo);
  vininfo->width = vin_info.info.width;
  vininfo->height = vin_info.info.height;
  vininfo->format = vin_info.info.format;
  vininfo->type = vin_info.info.type;
  vininfo->bits = vin_info.info.bits;
  vininfo->ratio = vin_info.info.ratio;
  vininfo->system = vin_info.info.system;
  vininfo->flip = vin_info.info.flip;
  vininfo->rotate = vin_info.info.rotate;
  return 0;
}

static int __s3l_get_stream_framefactor(int iav_fd, int index, SAmbaDSPStreamFramefactor *framefactor)
{
  struct iav_stream_cfg streamcfg;
  int ret = 0;
  if (DMaxStreamNumber <= index) {
    LOG_FATAL("index(%d) not as expected\n", index);
    return (-10);
  }
  memset(&streamcfg, 0, sizeof(streamcfg));
  streamcfg.id = index;
  streamcfg.cid = IAV_STMCFG_FPS;
  ret = ioctl(iav_fd, IAV_IOC_GET_STREAM_CONFIG, &streamcfg);
  if (0 > ret) {
    perror("IAV_IOC_GET_STREAM_CONFIG");
    LOG_ERROR("IAV_IOC_GET_STREAM_CONFIG fail, errno %d\n", errno);
    return ret;
  }
  framefactor->framefactor_num = streamcfg.arg.fps.fps_multi;
  framefactor->framefactor_den = streamcfg.arg.fps.fps_div;
  return 0;
}

static int __s3l_map_bsb(int iav_fd, SAmbaDSPMapBSB *map_bsb)
{
  int ret = 0;
  unsigned int map_size = 0;
  struct iav_querybuf querybuf;
  querybuf.buf = IAV_BUFFER_BSB;
  ret = ioctl(iav_fd, IAV_IOC_QUERY_BUF, &querybuf);
  if (0 > ret) {
    perror("IAV_IOC_QUERY_BUF");
    LOG_ERROR("IAV_IOC_QUERY_BUF fail, errno %d\n", errno);
    return ret;
  }
  map_bsb->size = querybuf.length;
  if (map_bsb->b_two_times) {
    map_size = querybuf.length * 2;
  } else {
    map_size = querybuf.length;
  }
  if (map_bsb->b_enable_read && map_bsb->b_enable_write) {
    map_bsb->base = mmap(NULL, map_size, PROT_WRITE | PROT_READ, MAP_SHARED, iav_fd, querybuf.offset);
  } else if (map_bsb->b_enable_read && !map_bsb->b_enable_write) {
    map_bsb->base = mmap(NULL, map_size, PROT_READ, MAP_SHARED, iav_fd, querybuf.offset);
  } else if (!map_bsb->b_enable_read && map_bsb->b_enable_write) {
    map_bsb->base = mmap(NULL, map_size, PROT_WRITE, MAP_SHARED, iav_fd, querybuf.offset);
  } else {
    LOG_ERROR("not read or write\n");
    return (-1);
  }
  if (map_bsb->base == MAP_FAILED) {
    perror("mmap");
    LOG_ERROR("mmap fail\n");
    return -1;
  }
  LOG_PRINTF("bsb_mem = %p, size = 0x%x\n", map_bsb->base, map_bsb->size);
  return 0;
}

static int __s3l_map_dsp(int iav_fd, SAmbaDSPMapDSP *map_dsp)
{
  TInt ret = 0;
  struct iav_querybuf querybuf;
  querybuf.buf = IAV_BUFFER_DSP;
  ret = ioctl(iav_fd, IAV_IOC_QUERY_BUF, &querybuf);
  if (DUnlikely(0 > ret)) {
    perror("IAV_IOC_QUERY_BUF");
    LOG_ERROR("IAV_IOC_QUERY_BUF fail, errno %d\n", errno);
    return ret;
  }
  map_dsp->size = querybuf.length;
  map_dsp->base = mmap(NULL, map_dsp->size, PROT_READ | PROT_WRITE, MAP_SHARED, iav_fd, querybuf.offset);
  if (DUnlikely(map_dsp->base == MAP_FAILED)) {
    perror("mmap");
    LOG_ERROR("mmap fail, errno %d\n", errno);
    return -1;
  }
  return 0;
}

static int __s3l_map_intrapb(int iav_fd, SAmbaDSPMapIntraPB *map_intrapb)
{
  TInt ret = 0;
  struct iav_querybuf querybuf;
  querybuf.buf = IAV_BUFFER_INTRA_PB;
  ret = ioctl(iav_fd, IAV_IOC_QUERY_BUF, &querybuf);
  if (DUnlikely(0 > ret)) {
    perror("IAV_IOC_QUERY_BUF");
    LOG_ERROR("IAV_IOC_QUERY_BUF fail, errno %d\n", errno);
    return ret;
  }
  map_intrapb->size = querybuf.length;
  map_intrapb->base = mmap(NULL, map_intrapb->size, PROT_READ | PROT_WRITE, MAP_SHARED, iav_fd, querybuf.offset);
  if (DUnlikely(map_intrapb->base == MAP_FAILED)) {
    perror("mmap");
    LOG_ERROR("mmap fail, errno %d\n", errno);
    return -1;
  }
  return 0;
}

static int __s3l_efm_get_bufferpool_info(int iav_fd, SAmbaEFMPoolInfo *buffer_pool_info)
{
  TInt ret = 0;
  struct iav_efm_get_pool_info efm_pool_info;
  memset(&efm_pool_info, 0, sizeof(efm_pool_info));
  ret = ioctl(iav_fd, IAV_IOC_EFM_GET_POOL_INFO, &efm_pool_info);
  if (0 > ret) {
    perror("IAV_IOC_EFM_GET_POOL_INFO");
    LOG_ERROR("IAV_IOC_EFM_GET_POOL_INFO fail, errno %d\n", errno);
    return ret;
  }
  if (buffer_pool_info) {
    buffer_pool_info->yuv_buf_num = efm_pool_info.yuv_buf_num;
    buffer_pool_info->yuv_pitch = efm_pool_info.yuv_pitch;
    buffer_pool_info->me1_buf_num = efm_pool_info.me_buf_num;
    buffer_pool_info->me1_pitch = efm_pool_info.me1_pitch;
    buffer_pool_info->yuv_buf_width = efm_pool_info.yuv_size.width;
    buffer_pool_info->yuv_buf_height = efm_pool_info.yuv_size.height;
    buffer_pool_info->me1_buf_width = efm_pool_info.me1_size.width;
    buffer_pool_info->me1_buf_height = efm_pool_info.me1_size.height;
  } else {
    LOG_FATAL("NULL param\n");
    return (-2);
  }
  return 0;
}

static int __s3l_efm_request_frame(int iav_fd, SAmbaEFMFrame *frame)
{
  TInt ret = 0;
  struct iav_efm_request_frame request;
  memset(&request, 0, sizeof(request));
  do {
    ret = ioctl(iav_fd, IAV_IOC_EFM_REQUEST_FRAME, &request);
    if (!ret) {
      break;
    }
    perror("IAV_IOC_EFM_REQUEST_FRAME");
    LOG_WARN("IAV_IOC_EFM_REQUEST_FRAME fail, errno %d, retry\n", errno);
    usleep(10000);
  } while (1);
  if (frame) {
    frame->frame_idx = request.frame_idx;
    frame->yuv_luma_offset = request.yuv_luma_offset;
    frame->yuv_chroma_offset = request.yuv_chroma_offset;
    frame->me1_offset = request.me1_offset;
  } else {
    LOG_FATAL("NULL param\n");
    return (-2);
  }
  return 0;
}

static int __s3l_efm_finish_frame(int iav_fd, SAmbaEFMFinishFrame *finish_frame)
{
  if (finish_frame) {
    TInt ret = 0;
    if (!finish_frame->b_not_wait_next_interrupt) {
      ret = ioctl(iav_fd, IAV_IOC_WAIT_NEXT_FRAME, 0);
      if (0 > ret) {
        perror("IAV_IOC_WAIT_NEXT_FRAME");
        LOG_ERROR("IAV_IOC_WAIT_NEXT_FRAME fail, errno %d\n", errno);
      }
    }
    struct iav_efm_handshake_frame handshake;
    memset(&handshake, 0, sizeof(handshake));
    handshake.frame_idx = finish_frame->frame_idx;
    handshake.frame_pts = finish_frame->frame_pts;
    do {
      ret = ioctl(iav_fd, IAV_IOC_EFM_HANDSHAKE_FRAME, &handshake);
      if (!ret) {
        break;
      }
      LOG_WARN("IAV_IOC_EFM_HANDSHAKE_FRAME fail, retry\n");
      usleep(10000);
    } while (1);
  } else {
    LOG_FATAL("NULL param\n");
    return (-2);
  }
  return 0;
}

static int __s3l_read_bitstream(int iav_fd, SAmbaDSPReadBitstream *bitstream)
{
  struct iav_querydesc query_desc;
  struct iav_framedesc *frame_desc;
  int ret = 0;
  memset(&query_desc, 0, sizeof(query_desc));
  frame_desc = &query_desc.arg.frame;
  query_desc.qid = IAV_DESC_FRAME;
  //frame_desc->id = bitstream->stream_id;
  frame_desc->id = 0xffffffff;
  frame_desc->time_ms = bitstream->timeout_ms;
  ret = ioctl(iav_fd, IAV_IOC_QUERY_DESC, &query_desc);
  if (ret) {
    if (EAGAIN == errno) {
      return 1;
    }
    perror("IAV_IOC_QUERY_DESC");
    LOG_ERROR("IAV_IOC_QUERY_DESC fail, errno %d\n", errno);
    return (-1);
  }
  bitstream->stream_id = frame_desc->id;
  bitstream->offset = frame_desc->data_addr_offset;
  bitstream->size = frame_desc->size;
  bitstream->pts = frame_desc->arm_pts;
  bitstream->video_width = frame_desc->reso.width;
  bitstream->video_height = frame_desc->reso.height;
  bitstream->slice_id = frame_desc->slice_id;
  bitstream->slice_num = frame_desc->slice_num;
  if (frame_desc->stream_end) {
    DASSERT(!bitstream->size);
    bitstream->size = 0;
    bitstream->offset = 0;
  }
  if (IAV_STREAM_TYPE_H264 == frame_desc->stream_type) {
    bitstream->stream_type = StreamFormat_H264;
  } else if (IAV_STREAM_TYPE_H265 == frame_desc->stream_type) {
    bitstream->stream_type = StreamFormat_H265;
  } else if (IAV_STREAM_TYPE_MJPEG == frame_desc->stream_type) {
    bitstream->stream_type = StreamFormat_JPEG;
  } else {
    bitstream->stream_type = StreamFormat_Invalid;
    LOG_FATAL("bad stream type %d\n", bitstream->stream_type);
    return (-2);
  }
  return 0;
}

static int __s3l_is_ready_for_read_bitstream(int iav_fd)
{
  return 1;
}

static int __s3l_intraplay_reset_buffers(int iav_fd, SAmbaDSPIntraplayResetBuffers *reset_buffers)
{
  TInt ret = 0;
  struct iav_intraplay_reset_buffers reset;
  memset(&reset, 0, sizeof(reset));
  reset.decoder_id = reset_buffers->decoder_id;
  reset.max_num = reset_buffers->max_num;
  ret = ioctl(iav_fd, IAV_IOC_INTRAPLAY_RESET_BUFFERS, &reset);
  if (ret) {
    perror("IAV_IOC_INTRAPLAY_RESET_BUFFERS");
    LOG_ERROR("IAV_IOC_INTRAPLAY_RESET_BUFFERS fail, errno %d\n", errno);
    return ret;
  }
  return 0;
}

static int __s3l_intraplay_request_buffer(int iav_fd, SAmbaDSPIntraplayBuffer *buffer)
{
  TInt ret = 0;
  struct iav_intraplay_frame_buffer buf;
  memset(&buf, 0, sizeof(buf));
  buf.ch_fmt = buffer->ch_fmt;
  buf.buf_width = buffer->buf_width;
  buf.buf_height = buffer->buf_height;
  ret = ioctl(iav_fd, IAV_IOC_INTRAPLAY_REQUEST_BUFFER, &buf);
  if (ret) {
    perror("IOC_INTRAPLAY_REQUEST_BUFFER");
    LOG_ERROR("IOC_INTRAPLAY_REQUEST_BUFFER fail, errno %d\n", errno);
    return ret;
  }
  buffer->buffer_id = buf.buffer_id;
  buffer->ch_fmt = buf.ch_fmt;
  buffer->buf_pitch = buf.buf_pitch;
  buffer->buf_width = buf.buf_width;
  buffer->buf_height = buf.buf_height;
  buffer->lu_buf_offset = buf.lu_buf_offset;
  buffer->ch_buf_offset = buf.ch_buf_offset;
  buffer->img_width = buf.img_width;
  buffer->img_height = buf.img_height;
  buffer->img_offset_x = buf.img_offset_x;
  buffer->img_offset_y = buf.img_offset_y;
  buffer->buffer_size = buf.buffer_size;
  return 0;
}

static int __s3l_intraplay_decode(int iav_fd, SAmbaDSPIntraplayDecode *decode)
{
  TInt ret = 0;
  TU32 i = 0;
  struct iav_intraplay_decode dec;
  memset(&dec, 0, sizeof(dec));
  dec.decoder_id = decode->decoder_id;
  dec.num = decode->num;
  dec.decode_type = decode->decode_type;
  for (i = 0; i < dec.num; i ++) {
    dec.bitstreams[i].bits_fifo_start = decode->bitstreams[i].bits_fifo_start;
    dec.bitstreams[i].bits_fifo_end = decode->bitstreams[i].bits_fifo_end;
    dec.buffers[i].buffer_id = decode->buffers[i].buffer_id;
    dec.buffers[i].ch_fmt = decode->buffers[i].ch_fmt;
    dec.buffers[i].buf_pitch = decode->buffers[i].buf_pitch;
    dec.buffers[i].buf_width = decode->buffers[i].buf_width;
    dec.buffers[i].buf_height = decode->buffers[i].buf_height;
    dec.buffers[i].lu_buf_offset = decode->buffers[i].lu_buf_offset;
    dec.buffers[i].ch_buf_offset = decode->buffers[i].ch_buf_offset;
    dec.buffers[i].img_width = decode->buffers[i].img_width;
    dec.buffers[i].img_height = decode->buffers[i].img_height;
    dec.buffers[i].img_offset_x = decode->buffers[i].img_offset_x;
    dec.buffers[i].img_offset_y = decode->buffers[i].img_offset_y;
    dec.buffers[i].buffer_size = decode->buffers[i].buffer_size;
  }
  ret = ioctl(iav_fd, IAV_IOC_INTRAPLAY_DECODE, &dec);
  if (ret) {
    perror("IAV_IOC_INTRAPLAY_DECODE");
    LOG_ERROR("IAV_IOC_INTRAPLAY_DECODE fail, errno %d\n", errno);
    return ret;
  }
  return 0;
}

static int __s3l_intraplay_yuv2yuv(int iav_fd, SAmbaDSPIntraplayYUV2YUV *yuv2yuv)
{
  TInt ret = 0;
  TU32 i = 0;
  struct iav_intraplay_yuv2yuv yy;
  memset(&yy, 0, sizeof(yy));
  yy.decoder_id = yuv2yuv->decoder_id;
  yy.num = yuv2yuv->num;
  yy.rotate = yuv2yuv->rotate;
  yy.flip = yuv2yuv->flip;
  yy.luma_gain = yuv2yuv->luma_gain;
  yy.src_buf.buffer_id = yuv2yuv->src_buf.buffer_id;
  yy.src_buf.ch_fmt = yuv2yuv->src_buf.ch_fmt;
  yy.src_buf.buf_pitch = yuv2yuv->src_buf.buf_pitch;
  yy.src_buf.buf_width = yuv2yuv->src_buf.buf_width;
  yy.src_buf.buf_height = yuv2yuv->src_buf.buf_height;
  yy.src_buf.lu_buf_offset = yuv2yuv->src_buf.lu_buf_offset;
  yy.src_buf.ch_buf_offset = yuv2yuv->src_buf.ch_buf_offset;
  yy.src_buf.img_width = yuv2yuv->src_buf.img_width;
  yy.src_buf.img_height = yuv2yuv->src_buf.img_height;
  yy.src_buf.img_offset_x = yuv2yuv->src_buf.img_offset_x;
  yy.src_buf.img_offset_y = yuv2yuv->src_buf.img_offset_y;
  yy.src_buf.buffer_size = yuv2yuv->src_buf.buffer_size;
  for (i = 0; i < yy.num; i ++) {
    yy.dst_buf[i].buffer_id = yuv2yuv->dst_buf[i].buffer_id;
    yy.dst_buf[i].ch_fmt = yuv2yuv->dst_buf[i].ch_fmt;
    yy.dst_buf[i].buf_pitch = yuv2yuv->dst_buf[i].buf_pitch;
    yy.dst_buf[i].buf_width = yuv2yuv->dst_buf[i].buf_width;
    yy.dst_buf[i].buf_height = yuv2yuv->dst_buf[i].buf_height;
    yy.dst_buf[i].lu_buf_offset = yuv2yuv->dst_buf[i].lu_buf_offset;
    yy.dst_buf[i].ch_buf_offset = yuv2yuv->dst_buf[i].ch_buf_offset;
    yy.dst_buf[i].img_width = yuv2yuv->dst_buf[i].img_width;
    yy.dst_buf[i].img_height = yuv2yuv->dst_buf[i].img_height;
    yy.dst_buf[i].img_offset_x = yuv2yuv->dst_buf[i].img_offset_x;
    yy.dst_buf[i].img_offset_y = yuv2yuv->dst_buf[i].img_offset_y;
    yy.dst_buf[i].buffer_size = yuv2yuv->dst_buf[i].buffer_size;
  }
  ret = ioctl(iav_fd, IAV_IOC_INTRAPLAY_YUV2YUV, &yy);
  if (ret) {
    perror("IAV_IOC_INTRAPLAY_YUV2YUV");
    LOG_ERROR("IAV_IOC_INTRAPLAY_YUV2YUV fail, errno %d\n", errno);
    return ret;
  }
  return 0;
}

static int __s3l_intraplay_display(int iav_fd, SAmbaDSPIntraplayDisplay *display)
{
  TInt ret = 0;
  TU32 i = 0;
  struct iav_intraplay_display d;
  memset(&d, 0, sizeof(d));
  d.decoder_id = display->decoder_id;
  d.num = display->num;
  for (i = 0; i < d.num; i ++) {
    d.buffers[i].buffer_id = display->buffers[i].buffer_id;
    d.buffers[i].ch_fmt = display->buffers[i].ch_fmt;
    d.buffers[i].buf_pitch = display->buffers[i].buf_pitch;
    d.buffers[i].buf_width = display->buffers[i].buf_width;
    d.buffers[i].buf_height = display->buffers[i].buf_height;
    d.buffers[i].lu_buf_offset = display->buffers[i].lu_buf_offset;
    d.buffers[i].ch_buf_offset = display->buffers[i].ch_buf_offset;
    d.buffers[i].img_width = display->buffers[i].img_width;
    d.buffers[i].img_height = display->buffers[i].img_height;
    d.buffers[i].img_offset_x = display->buffers[i].img_offset_x;
    d.buffers[i].img_offset_y = display->buffers[i].img_offset_y;
    d.buffers[i].buffer_size = display->buffers[i].buffer_size;
    d.desc[i].vout_id = display->desc[i].vout_id;
    d.desc[i].vid_win_update = display->desc[i].vid_win_update;
    d.desc[i].vid_win_rotate = display->desc[i].vid_win_rotate;
    d.desc[i].vid_flip = display->desc[i].vid_flip;
    d.desc[i].vid_win_width = display->desc[i].vid_win_width;
    d.desc[i].vid_win_height = display->desc[i].vid_win_height;
    d.desc[i].vid_win_offset_x = display->desc[i].vid_win_offset_x;
    d.desc[i].vid_win_offset_y = display->desc[i].vid_win_offset_y;
  }
  ret = ioctl(iav_fd, IAV_IOC_INTRAPLAY_DISPLAY, &d);
  if (ret) {
    perror("IAV_IOC_INTRAPLAY_DISPLAY");
    LOG_ERROR("IAV_IOC_INTRAPLAY_DISPLAY fail, errno %d\n", errno);
    return ret;
  }
  return 0;
}

static void __setup_s3l_al_context(SFAmbaDSPDecAL *al)
{
  al->f_enter_mode = __s3l_enter_decode_mode;
  al->f_leave_mode = __s3l_leave_decode_mode;
  al->f_create_decoder = __s3l_create_decoder;
  al->f_destroy_decoder = __s3l_destroy_decoder;
  al->f_trickplay = __s3l_decode_trick_play;
  al->f_start = __s3l_decode_start;
  al->f_stop = __s3l_decode_stop;
  al->f_speed = __s3l_decode_speed;
  al->f_request_bsb = __s3l_decode_request_bits_fifo;
  al->f_decode = __s3l_decode;
  al->f_query_print_decode_bsb_status = __s3l_decode_query_bsb_status_and_print;
  al->f_query_print_decode_status = __s3l_decode_query_status_and_print;
  al->f_query_decode_bsb_status = __s3l_decode_query_bsb_status;
  al->f_query_decode_status = __s3l_decode_query_status;
  al->f_get_vout_info = __s3l_get_single_vout_info;
  al->f_get_vin_info = __s3l_get_vin_info;
  al->f_get_stream_framefactor = __s3l_get_stream_framefactor;
  al->f_map_bsb = __s3l_map_bsb;
  al->f_map_dsp = __s3l_map_dsp;
  al->f_map_intrapb = __s3l_map_intrapb;
  al->f_efm_get_buffer_pool_info = __s3l_efm_get_bufferpool_info;
  al->f_efm_request_frame = __s3l_efm_request_frame;
  al->f_efm_finish_frame = __s3l_efm_finish_frame;
  al->f_read_bitstream = __s3l_read_bitstream;
  al->f_is_ready_for_read_bitstream = __s3l_is_ready_for_read_bitstream;
  al->f_intraplay_reset_buffers = __s3l_intraplay_reset_buffers;
  al->f_intraplay_request_buffer = __s3l_intraplay_request_buffer;
  al->f_intraplay_decode = __s3l_intraplay_decode;
  al->f_intraplay_yuv2yuv = __s3l_intraplay_yuv2yuv;
  al->f_intraplay_display = __s3l_intraplay_display;
}

#endif

#endif

int gfOpenIAVHandle()
{
#ifdef BUILD_MODULE_AMBA_DSP
  int fd = open("/dev/iav", O_RDWR, 0);
  if (0 > fd) {
    LOG_FATAL("open iav fail, %d.\n", fd);
  }
  return fd;
#endif
  LOG_FATAL("dsp related is not compiled\n");
  return (-4);
}

void gfCloseIAVHandle(int fd)
{
#ifdef BUILD_MODULE_AMBA_DSP
  if (0 > fd) {
    LOG_FATAL("bad fd %d\n", fd);
    return;
  }
  close(fd);
  return;
#endif
  LOG_FATAL("dsp related is not compiled\n");
  return;
}

void gfSetupDSPAlContext(SFAmbaDSPDecAL *al)
{
#ifdef BUILD_MODULE_AMBA_DSP
#ifdef BUILD_DSP_AMBA_S2L
  __setup_s2l_al_context(al);
#elif defined (BUILD_DSP_AMBA_S2) || defined (BUILD_DSP_AMBA_S2E)
  __setup_s2_s2e_al_context(al);
#elif defined (BUILD_DSP_AMBA_S3L)
  __setup_s3l_al_context(al);
#else
  LOG_FATAL("add support here\n");
#endif
  return;
#endif
  LOG_FATAL("dsp related is not compiled\n");
  return;
}

void gfFillAmbaGopHeader(TU8 *p_gop_header, TU32 frame_tick, TU32 time_scale, TU32 pts, TU8 gopsize, TU8 m)
{
  TU32 tick_high = frame_tick;
  TU32 tick_low = tick_high & 0x0000ffff;
  TU32 scale_high = time_scale;
  TU32 scale_low = scale_high & 0x0000ffff;
  TU32 pts_high = 0;
  TU32 pts_low = 0;
  tick_high >>= 16;
  scale_high >>= 16;
  p_gop_header[0] = 0; // start code prefix
  p_gop_header[1] = 0;
  p_gop_header[2] = 0;
  p_gop_header[3] = 1;
  p_gop_header[4] = 0x7a; // nal type
  p_gop_header[5] = 0x01; // version main
  p_gop_header[6] = 0x01; // version sub
  p_gop_header[7] = tick_high >> 10;
  p_gop_header[8] = tick_high >> 2;
  p_gop_header[9] = (tick_high << 6) | (1 << 5) | (tick_low >> 11);
  p_gop_header[10] = tick_low >> 3;
  p_gop_header[11] = (tick_low << 5) | (1 << 4) | (scale_high >> 12);
  p_gop_header[12] = scale_high >> 4;
  p_gop_header[13] = (scale_high << 4) | (1 << 3) | (scale_low >> 13);
  p_gop_header[14] = scale_low >> 5;
  p_gop_header[15] = (scale_low << 3) | (1 << 2) | (pts_high >> 14);
  p_gop_header[16] = pts_high >> 6;
  p_gop_header[17] = (pts_high << 2) | (1 << 1) | (pts_low >> 15);
  p_gop_header[18] = pts_low >> 7;
  p_gop_header[19] = (pts_low << 1) | 1;
  p_gop_header[20] = gopsize;
  p_gop_header[21] = (m & 0xf) << 4;
}

void gfUpdateAmbaGopHeader(TU8 *p_gop_header, TU32 pts)
{
  TU32 pts_high = (pts >> 16) & 0x0000ffff;
  TU32 pts_low = pts & 0x0000ffff;
  p_gop_header[15] = (p_gop_header[15]  & 0xFC) | (pts_high >> 14);
  p_gop_header[16] = pts_high >> 6;
  p_gop_header[17] = (pts_high << 2) | (1 << 1) | (pts_low >> 15);
  p_gop_header[18] = pts_low >> 7;
  p_gop_header[19] = (pts_low << 1) | 1;
}

unsigned int gfGetMaxEncodingStreamNumber()
{
#ifdef BUILD_MODULE_AMBA_DSP
  return IAV_STREAM_MAX_NUM_ALL;
#endif
  LOG_FATAL("dsp related is not compiled\n");
  return 0;
}

